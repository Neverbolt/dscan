# dscan

## About
`dscan` is a tool used to scan a large amount of domains against a large amount of tests.

All scanners are categorically sorted in the scanner folder and are registered via the `scanners.go` file.

## Usage
```
NAME:
   dscan - check a list of domains against certain tests

USAGE:
   dscan [command options] domainfile [domainfile2...]

VERSION:
   0.92

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --connections value, -c value  number of simultaneous connections (default: 1)
   --all, -a                      run all scans that don't require additional input
   --verbose, --vv                print verbose output
   --debug, --vvvv                print very verbose output
   --server                       start in server mode (uses other settings as default and does not use domainfiles), allows executing arbitrary scripts as the user running the server, use with care!
   --merge                        merge a set of result files which have been run with differing settings / versions of the software, behaves like the prepend flag but does not scan and therefore needs no domain file
   --useragent value              user agent used for all connections
   --resolver value               dns resolver used for all dns queries (except for http request resolution) (default: "8.8.8.8")
   --status                       display status on stderr
   --scriptdir value              location of the scripts with relative paths (also of the local ssl check script), if not set tries 'scripts/' both in the current directory as well as in the location of the executable (default: "scripts")
   --prepend value                prepends an existing CSV file to the result, matching and adjusting column offsets so that no longer needed columns are filled empty and new columns are appended at the end (not supported in server mode)
   --ca                           check certificates
   --ciphercheck                  checks if the used cipher is in a list of secure ciphers, a very very much simplified version of ssl rating
   --cms                          scan for cms by directory "fuzzing"
   --content-length               returns the content length of the HTTP response, since golang does not show it as Header for the header scanner
   --csp                          checks if a content security policy can be found in the HTTP headers or as meta tag and returns it with the applicable mode
   --dns value                    load dns entriy for final url
   --run value                    runs external scripts that are passed DOMAIN and URL as parameter (ignores stderr)
   --find value                   checks if certain elements and texts are present on the index site
   --header value                 extract http header
   --host                         resolve the original hoster (aws, gcloud, ...)
   --redirect value               takes "all" or "brief", and "host", lists the redirect steps and protocols
   --sechead                      tests for security header rating
   --ssl value                    tests for ssl rating, takes "qualys" or "local" as parameter
   --startdate value              adds a column containing the starting date in a given format (use 'simple' for DD.MM.YYYY, otherwise see https://golang.org/pkg/time/#Time.Format)
   --subdomaintakeover value      checks if a subdomain is pointing to a public provider where an attacker can takeover it, takes "brief" or "detailed" as parameter
   --help, -h                     show help
   --version, -v                  print the version
```

### Domain input
Domains are accepted via a (csv) file that is passed as last parameter.

The file should have one domain per line and can have further information, like header values, cookies and variables to be substituted in other parts of the program.

When a variable is not given the rule that would contain the variable is ignored. (as of now only the finder supports variables, the cookie variable seen below is a not-yet supported example)

Examples for this are:

```
google.com;var:"img_url:test.png"
example.com;cookie:"PHPSESSID:fpebefdltu8t4d28kfc9nevrg1,admin:1";var:"img_url:construction.jpg"
icetee.org
```

### Server Mode
When starting server mode the access key is printed to `stdout`. This key is important, since otherwise `dscan` can be used to read and even execute arbitrary files which would be bad.

Server mode starts a webserver at port `:9999` and registers the endpoint `/scan/{access key}`.

This can then be used by `POST`ing a `JSON` struct similar to the following:

```
{
    "domains": ["myspace.com", "yahoo.de", "..."],
    "config": {
        "find": ["lists/googleads.txt"],
        "cms": true,
        "host": true,
        "run": ["scripts/nmap.sh", "scripts/blindelephant.py"]
    }
}
```

Which will return a list of `JSON` structs with the results.


### Merge Mode
Merge mode is used to merge multiple scan results with different formats or from different dscan versions to get a uniform data format. *When starting merge mode all other flags are ignored and no scan is actually done!*

Merge mode takes as parameters a list of files that contains results, which are passed instead of the usual domain file(s).

It then loads their headers and calculates a transformation which results in the format of beginning with all fields of the first file, followed by all fields that are not in the first but in the second and so on. This can be visualized as such:

```
header1: 1;2;3;5
header2: 2;4;3
header3: 1;5;8

result:  1;2;3;5;4;8
```

The combined header is then output, followed by the contents of the files in order, which are transformed by the previously calculated transformations. Fields that are not present in a file are left empty, ones that are in another position are moved to their new location. Building on the previous example:

```
header1:  1;2;3;5
content1: 1;2;3;4
header2:  2;4;3
content2: 1;2;3
header3:  1;5;8
content3: 1;2;3

result (x represents an empty field in this example, in the real output it is simply left empty):
(header)   1;2;3;5;4;8
(content1) 1;2;3;4;x;x
(content2) x;1;3;x;2;x
(content3) 1;x;x;2;x;3
```

This guarantees that downstream software can rely on the first fields always staying the same.


### User Agent
The user agent can be freely set, to allow certain WAF bypasses. It can not yet be dynamically set by parameter for each domain, however this is planned for later.

### Resolver
TODO

### Status
Status will additionally print the number of already scanned domains to stderr after each domain to allow quick status checking.

### Scriptdir
TODO

### Prepend
The prepend parameter takes the path to a file that contains the previous results and merges it with the current output in the same way that the merge mode works.

This can be done to keep the same output format even when options, or the dscan version changes. One should not try to prepend a file where the current output is piped into, since this can lead to empty or infinite results, depending on if the file is written or appended to.

### CA
The CA flag enables resolution of the certificate organization and the certificate ca organization.

### Cipher Check
The Cipher Check is an extremely primitive way to check only the SSL chipher, by matching the used one against a list of known secure ciphers and returning `secure` if it is contained and `insecure` otherwise.

### CMS
CMS does very primitive CMS detection, by comparing a few random static files. It is very very weak and unreliable and only works under optimal conditions.

### Content-Length
This simply returns the Content-Length of the final page, which can not always be gotten simply from the header.

It can be used to detect exact copies of a page, although a better matching is in the works.

### CSP
TODO

### DNS

The DNS scanner allows requesting any information that resides in the domain name server.

The list of supported entries is:

```
A, NS, MD, MF, CNAME, SOA, MB, MG, MR, PTR,
HINFO, MINFO, MX, TXT, RP, AFSDB, X25, RT,
NSAPPTR, SIG, KEY, PX, GPOS, AAAA, LOC, EID,
NIMLOC, SRV, NAPTR, KX, CERT, DNAME, DS,
SSHFP, RRSIG, NSEC, DNSKEY, DHCID, NSEC3,
NSEC3PARAM, TLSA, SMIMEA, HIP, NINFO, RKEY,
TALINK, CDS, CDNSKEY, OPENPGPKEY, CSYNC,
SPF, UINFO, UID, GID, NID, L32, L64, LP,
EUI48, EUI64, URI, CAA, AVC
```

### Scripts

To run a script for each domain it can simply be added to the scanlist via the `--run scripts/nmap.sh` parameter. The only difference between scripts and built in scanners is, that scripts get run sequentially for each domain, whereas golang scanners run fully parallel.

This means that while the domain `example.com` may be scanned by all tools simultaneously, there is only at most one script running on it.

Be aware that even though only one script per domain is running, however, a script may be running multiple times at once over multiple domains.

### Find
The finder allows for fuzzy and exact finding of text in a website. For this only the default page is used and JS is (not yet) evaluated.

To use it you supply the file that contains the finder rules. These should look like so:

```
{maintenance}
img[src] ~ server_maintenance
img[src] ~ $img_url

{interesting}
body ~ Unauthorized
body ~ don't have permission to access
body ~ authorized to access the document requested

{login}
input[type] = password
```

Curly braces are categories that are displayed as output, so if `Unauthorized` is anywhere in the body the `interesting` tag is added. When the first rule of a category matches the others are no longer evaluated since they wouldn't change anything.

Each category can have many matchings which are comprised of a css selector (similar to jquery), an optional attribute selector in the `[]` braces, a matching variant (`=` for exact and `~` for fuzzy matching) and a matching text.

Each part can contain variables that are evaluated on a per-domain basis.

### Header
The header mode extracts all given fields from the return header and puts them in their own output fields.
Testing `google.com` for `Server` and `Content-Type` yields:

```
$ dscan --header Server --header Content-Type domains.txt
domain;success;Server;Content-Type
google.com;success;gws;"text/html; charset=ISO-8859-1"
```

### Host
Host prints both the IP address of the server as well as the reverse IP lookup, from which the hoster can be detected.

### Redirect
Redirect resolves redirections that come from visiting the domains. It takes either the `all` or `brief` flag, with which either all step or only the last destination will be printed.
Additionally you can give the `host` flag, which will then only print the hostnames.

The flags can be combined like so:
```
$ dscan --redirect all --redirect host domains.txt
domain;success;redirects;scheme;status
google.com;success;google.com,www.google.com;https;200
```

### Security Headers
The security header rating checks for the following header values being set and awards a point for each.
- `content-security-policy` or `x-frame-options`
- `x-xss-protection`
- `x-content-type-options`
- `referrer-policy`
- `feature-policy`
- `strict-transport-security`
The points are then put on the scale of [`F`, `E`, `D`, `C`, `B`, `A`, `A+`]

### SSL
The SSL flag queries either the Qualys SSL Labs API or a local script found at `scripts/sslgrade.py` for the given grade of the domain.

The Qualys evaluation takes a very long time and should be done in a paralell run to have substainable speed, since each Qualys evaluation takes approximately one minute and therefore quickly hogs worker processes, since it also has a limit on simultaneous connections.

The local evaluation is a bit more sloppy and not as thorough, but much much faster, without rate limits and taking ~5-10 seconds depending on the connection.

Possible grades are A+, A-, A-F, T (no trust) and M (certificate name mismatch).

### Subdomain takeover
The subdomain takeover flag enables a scan that tries to find out if it is possible to takeover a subdomain which is 
pointing to a public service provider using a cname record. It will also try to check if the cname is pointing to an
IP address (IPv4 and IPv6).

The parameter "brief" returns information about the detected provider and if it is possible to takeover a domain.
Using the parameter "detailed" it is possible to get more information about an error that occurred or how the possible takeover got detected.

The value of a detected provider can be ether the name of the provider or "UNKNOWN" if the scanner cannot detect the provider.

Possible takeover values are "yes", "no" or "maybe".  "Maybe" states that it might be possible to takeover, but it needs an investigation by a human.

More information about the subdomain takeover topic: https://glm.io/135513

## Development
`dscan` allows a large range of functionality, especially since it can easily be extended.

### Extending the core scanners

To create a new golang scanner one can simply create a new file in any of the `scanners/*` directories and then register it via `scanners.go`.

To do that a name and description are needed, as well as a scanner function and the fields that it returns. A scanner function always has to return the same number of results as there are fields registered by it, otherwise the scan will fail and the program will abort.

For more elaborate scanners an init method can also be provided, which is called once at the start of the program (Scanners are only initialized if they are in use). To allow for input the cli flag can also be changed to `BoolFlag`, `StringFlag` or `StringSliceFlag`.

### Building

To build `dscan` you need to have `go` and `dep` installed. To install go follow the latest guidelines, to install dep you can do `go install github.com/golang/dep`.

To install all dependencies just do a `dep ensure` and then you can build and install dscan with `go install .`.

If you do not want to build `dscan` yourself then you can use the system appropriate binary which is provided in the `bin` folder of this repository.

## TODO

- crawler
- javascript
