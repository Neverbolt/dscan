package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli"

	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/scanner"
	"gitlab.com/Neverbolt/dscan/server"

	log "github.com/Sirupsen/logrus"

	_ "gitlab.com/Neverbolt/dscan/scanner/scanners" // register the scanners
)

func main() {
	// Create CLI App
	app := cli.NewApp()
	app.Name = "dscan"
	app.Version = "0.92"
	app.Usage = "check a list of domains against certain tests"
	app.UsageText = "dscan [command options] domainfile [domainfile2...]"
	app.Before = initParams
	app.Action = start
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "all",
			Usage: "run all tests",
		},
	}

	// Add global flags
	app.Flags = []cli.Flag{
		cli.UintFlag{
			Name:  "connections, c",
			Usage: "number of simultaneous connections",
			Value: 1,
		},
		cli.BoolFlag{
			Name:  "all, a",
			Usage: "run all scans that don't require additional input",
		},
		cli.BoolFlag{
			Name:  "verbose, vv",
			Usage: "print verbose output",
		},
		cli.BoolFlag{
			Name:  "debug, vvvv",
			Usage: "print very verbose output",
		},
		cli.BoolFlag{
			Name:  "server",
			Usage: "start in server mode (uses other settings as default and does not use domainfiles), allows executing arbitrary scripts as the user running the server, use with care!",
		},
		cli.BoolFlag{
			Name:  "merge",
			Usage: "merge a set of result files which have been run with differing settings / versions of the software, behaves like the prepend flag but does not scan and therefore needs no domain file",
		},
		cli.StringFlag{
			Name:  "useragent",
			Usage: "user agent used for all connections",
		},
		cli.StringFlag{
			Name:  "resolver",
			Usage: "dns resolver used for all dns queries (except for http request resolution)",
			Value: "8.8.8.8",
		},
		cli.BoolFlag{
			Name:  "status",
			Usage: "display status on stderr",
		},
		cli.StringFlag{
			Name:  "scriptdir",
			Usage: "location of the scripts with relative paths (also of the local ssl check script), if not set tries 'scripts/' both in the current directory as well as in the location of the executable",
			Value: "scripts",
		},
		cli.StringFlag{
			Name: "prepend",
			Usage: "prepends an existing CSV file to the result, matching and adjusting column offsets so that no longer needed columns are filled empty and new columns are appended at the end (not supported in server mode)",
		},
	}

	// Add scanner flags
	for _, s := range scanner.Scanners {
		if s.Flag == config.BoolFlag {
			app.Flags = append(app.Flags, cli.BoolFlag{
				Name:  s.Name,
				Usage: s.Desc,
			})
		} else if s.Flag == config.StringFlag {
			app.Flags = append(app.Flags, cli.StringFlag{
				Name:  s.Name,
				Usage: s.Desc,
			})
		} else if s.Flag == config.StringSliceFlag {
			app.Flags = append(app.Flags, cli.StringSliceFlag{
				Name:  s.Name,
				Usage: s.Desc,
			})
		} else {
			log.Fatalf("Unknown flag type %d for %s", s.Flag, s.Name)
		}
	}

	// Run app
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

// initParams checks all parameters for validity and starts the reader and writer routines
func initParams(c *cli.Context) error {
	// the verbose flag enables the logger
	log.SetOutput(os.Stderr)
	if c.GlobalBool("debug") {
		log.SetLevel(log.DebugLevel)
	} else if c.GlobalBool("verbose") {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.ErrorLevel)
	}

	if c.Bool("server") {
		return nil
	}

	return initScanParams(c)
}

func initScanParams(c *cli.Context) error {
	// Check if at least one domain path is given
	if c.NArg() == 0 {
		if c.Bool("merge") {
			return fmt.Errorf("no merge file path given")
		}
		return fmt.Errorf("no domain file path given")
	}

	// Get all files and check if they exist/can be opened
	for _, file := range c.Args() {
		stat, err := os.Stat(file)
		if err != nil {
			return fmt.Errorf("could not open file \"%s\": %s", file, err)
		}

		if stat.IsDir() {
			return fmt.Errorf("could not open file \"%s\": Is a directory", file)
		}
	}

	return nil
}

// start hands the control to the actual starting method
func start(c *cli.Context) error {
	if c.Bool("merge") {
		return mergeFiles(c)
	}

	if c.Bool("server") {
		return startServer(c)
	}

	return startScan(c)
}

func mergeFiles(c *cli.Context) error {
	return config.MergeFiles(c.Args())
}

// startServer loads the server module and hands over control
func startServer(c *cli.Context) error {
	return server.RegisterEndpoints(c)
}

// startScan starts the cli based scan
func startScan(c *cli.Context) error {
	// Create the input and output connections
	// inlet is from the domain file streams to the scanners
	inlet := make(chan string, c.Int("connections")*2)
	// outlet is from the scanners to the output stream
	outlet := make(chan *[]string, c.Int("connections")*2)
	// writerDone is closed as soon as the last entry has been written by WriteOut
	writerDone := make(chan bool, 1)

	// Start the IO goroutines
	go config.ReadFiles(c.Args(), inlet)
	go config.WriteOut(outlet, writerDone, c.String("prepend"))

	err := scanner.Scan(c, inlet, outlet)
	if err != nil {
		return err
	}

	// close outlet and wait for it to be emptied
	close(outlet)
	<-writerDone

	return nil
}
