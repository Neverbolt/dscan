#!/usr/bin/env python3

from sslyze.server_connectivity_tester import ServerConnectivityTester
from sslyze.synchronous_scanner import SynchronousScanner
from sslyze.plugins.openssl_cipher_suites_plugin import Sslv20ScanCommand, Sslv30ScanCommand, Tlsv10ScanCommand, Tlsv11ScanCommand, Tlsv12ScanCommand, Tlsv13ScanCommand
from sslyze.plugins.certificate_info_plugin import CertificateInfoScanCommand

import cryptography
import cryptography.hazmat.backends.openssl
from cryptography.hazmat.primitives.serialization import Encoding

import sys

grades = [
    (  0,  20, "F"),
    ( 20,  35, "E"),
    ( 35,  50, "D"),
    ( 50,  65, "C"),
    ( 65,  80, "B"),
    ( 80, 100, "A"),
]

category_weights = {
    "protocol_support": 30,
    "key_exchange": 30,
    "cipher_strength": 40,
}

protocol_support_weights = {
    "SSLv2.0": (Sslv20ScanCommand, 0),
    "SSLv3.0": (Sslv30ScanCommand, 80),
    "TLSv1.0": (Tlsv10ScanCommand, 90),
    "TLSv1.1": (Tlsv11ScanCommand, 95),
    "TLSv1.2": (Tlsv12ScanCommand, 100),
    "TLSv1.3": (Tlsv13ScanCommand, 100),
}

key_exchange_weights = [
    (   0,     1,   0),
    (   0,   512,  20),
    ( 512,  1024,  40),
    (1024,  2048,  80),
    (2048,  4096,  90),
    (4096, 2**16, 100),
]

cipher_strength_weights = [
    (  0,     1,   0),
    (  0,   128,  20),
    (128,   256,  80),
    (256, 2**12, 100),
]

def scan_server(addr):
    server = ServerConnectivityTester(hostname=addr).perform()
    scanner = SynchronousScanner()
    score = 0

    protocol_score, ciphers = scan_protocol_support(server, scanner)
    key_exchange_score = scan_key_exchange(server, scanner)
    cipher_strength_score = get_cipher_strength(ciphers)

    score += category_weights["protocol_support"] * protocol_score
    score += category_weights["key_exchange"]     * key_exchange_score
    score += category_weights["cipher_strength"]  * cipher_strength_score

    return score // 100


def scan_protocol_support(server, scanner):
    ciphers = []

    lowest = -1
    highest = -1

    for _, (c, s) in protocol_support_weights.items():
        result = scanner.run_scan_command(server, c(hide_rejected_ciphers=True))
        if len(result.accepted_cipher_list) > 0:
            ciphers.extend(result.accepted_cipher_list)
            if lowest < 0:
                lowest = s
            highest = s

    return (lowest + highest) // 2, ciphers


def scan_key_exchange(server, scanner):
    result = scanner.run_scan_command(server, CertificateInfoScanCommand())
    chain = result.received_certificate_chain

    leaf = parse_cert(chain[0])
    leaf_key = leaf.public_key()

    key_size = 0
    if hasattr(leaf_key, "key_size"):
        key_size = leaf_key.key_size
    elif hasattr(leaf_key, "curve"):
        key_size = leaf_key.curve.key_size

    for (l, u, s) in key_exchange_weights:
        if l <= key_size and key_size < u:
            return s


def get_cipher_strength(ciphers):
    lowest = 0
    highest = 0

    for c in ciphers:
        lowest = min(lowest, c.key_size)
        highest = max(highest, c.key_size)

    key_size = (lowest + highest) // 2
    for (l, u, s) in cipher_strength_weights:
        if l <= key_size and key_size < u:
            return s


def parse_cert(cert):
    backend = cryptography.hazmat.backends.openssl.backend
    pem_bytes = cert.public_bytes(Encoding.PEM).decode('ascii').encode('utf-8')
    return cryptography.x509.load_pem_x509_certificate(pem_bytes, backend)


if __name__ == "__main__":
    try:
        score = scan_server(sys.argv[1])
        for (l, u, s) in grades:
            if l <= score and score < u:
                print(s)
                break
    except Exception as e:
        print("X")
        sys.exit(1)