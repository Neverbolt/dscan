package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/Neverbolt/dscan/config"

	log "github.com/Sirupsen/logrus"
)

// ServiceFunction is a function that can be converted to a WrappedServiceFunction by ServiceWrapper
type ServiceFunction func(http.ResponseWriter, *http.Request, config.Config)

// WrappedServiceFunction is a function that is ready to be passed to mux.Router.HandleFunc
type WrappedServiceFunction func(http.ResponseWriter, *http.Request)

// ServiceWrapper adds the current configuration to the web request and checks it for the authentication key
func ServiceWrapper(name string, fun ServiceFunction, conf config.Config) WrappedServiceFunction {
	return func(w http.ResponseWriter, r *http.Request) {
		timestart := time.Now()
		log.Infof("Started request for %s", name)

		w.Header().Set("Content-Type", "application/json")
		fun(w, r, conf)

		log.Infof("Finished request for %s in %f ms", name, float64(time.Since(timestart).Nanoseconds())*1e-6)
	}
}

type reply struct {
	Success bool                   `json:"success"`
	Message string                 `json:"message,omitempty"`
	Data    map[string]interface{} `json:"data,omitempty"`
}

// replyError writes success: false as well as a custom error message to the output stream
func replyError(w http.ResponseWriter, message string) {
	w.WriteHeader(500)
	sendReply(w, fail(message, message))
}

// replyData writes success: true as well as data to the output stream
func replyData(w http.ResponseWriter, data map[string]interface{}) {
	sendReply(w, success(data))
}

// error fills the error message into a reply struct representing a failed API call
func fail(messageCode string, message string, messageArgs ...interface{}) *reply {
	log.Errorf(message, messageArgs...)
	return &reply{false, message, nil}
}

// success fills the data into a reply struct representing a successfull API call
func success(data ...map[string]interface{}) *reply {
	if len(data) == 0 {
		return &reply{true, "", map[string]interface{}{}}
	} else if len(data) > 1 {
		log.Errorf("Trying to write %d results on success, can only take one!", len(data))
	}

	return &reply{true, "", data[0]}
}

// sendReply should not be used outside utils
func sendReply(w http.ResponseWriter, r *reply) {
	out, err := json.Marshal(r)
	if err != nil {
		log.Errorf("Error while encoding reply %+v to json! %s", r, err)
		out, err = json.Marshal(&reply{false, "MarshalDataError", nil})
		if err != nil {
			log.Fatalf("Can not marshal fallback reply data")
		}
	}

	n, err := fmt.Fprintf(w, string(out))
	if err != nil {
		log.Errorf("Error while sending result to client after %d bytes: %s", n, err)
	}
}
