package server

import (
	"crypto/rand"
	"encoding/base32"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"

	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/scanner"

	log "github.com/Sirupsen/logrus"
)

// RegisterEndpoints registers all endpoints and protects them with the passed key
func RegisterEndpoints(defaultConfig config.Config) error {
	rawKey := make([]byte, 15)

	_, err := rand.Read(rawKey)
	if err != nil {
		return err
	}

	key := base32.StdEncoding.EncodeToString(rawKey)

	fmt.Println(key)

	r := http.NewServeMux()

	r.HandleFunc("/scan/"+key, ServiceWrapper("scan", scanHandler, defaultConfig))

	return http.ListenAndServe(":9999", r)
}

func scanHandler(w http.ResponseWriter, r *http.Request, c config.Config) {
	inlet := make(chan string, c.Int("connections")*2)
	outlet := make(chan *[]string, c.Int("connections")*2)

	req := &request{config: c}
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		log.Errorf("Could not unmarshal request")
		replyError(w, "ConfigurationError")
		return
	}

	wg := &sync.WaitGroup{}

	wg.Add(2)
	go getDomains(req, inlet, wg)
	go writeJSONReply(w, outlet, wg)

	err := scanner.Scan(req, inlet, outlet)
	if err != nil {
		log.Errorf("Failed webscan: %s", err)
	}

	close(outlet)

	wg.Wait()
}

func writeJSONReply(w http.ResponseWriter, outlet <-chan *[]string, wg *sync.WaitGroup) {
	defer wg.Done()

	headers := *<-outlet

	enc := json.NewEncoder(w)
	for outline := range outlet {
		m := map[string]string{}

		for i, v := range *outline {
			m[headers[i]] = v
		}

		if err := enc.Encode(m); err != nil {
			log.Errorf("Could not write json reply: %s", err)
		}
	}
}

func getDomains(r *request, inlet chan<- string, wg *sync.WaitGroup) {
	defer wg.Done()

	// Put all received domains into the inlet
	for _, d := range r.Domains {
		inlet <- d
	}

	// Close the inlet connection to let other parts know that it's over
	close(inlet)
}
