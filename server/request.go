package server

import (
	"encoding/json"

	"gitlab.com/Neverbolt/dscan/config"
)

type request struct {
	Format  string                     `json:"format"`
	Domains []string                   `json:"domains"`
	Config  map[string]json.RawMessage `json:"config"`
	config  config.Config
}

func (r *request) Int(name string) int {
	v, ok := r.Config[name]
	if !ok {
		if r.config != nil {
			return r.config.Int(name)
		}
		return 0
	}

	var i int
	if err := json.Unmarshal(v, &i); err != nil {
		if r.config != nil {
			return r.config.Int(name)
		}
		return 0
	}

	return i
}

func (r *request) Bool(name string) bool {
	v, ok := r.Config[name]
	if !ok {
		if r.config != nil {
			return r.config.Bool(name)
		}
		return false
	}

	var b bool
	if err := json.Unmarshal(v, &b); err != nil {
		if r.config != nil {
			return r.config.Bool(name)
		}
		return false
	}

	return b
}

func (r *request) String(name string) string {
	v, ok := r.Config[name]
	if !ok {
		if r.config != nil {
			return r.config.String(name)
		}
		return ""
	}

	var s string
	if err := json.Unmarshal(v, &s); err != nil {
		if r.config != nil {
			return r.config.String(name)
		}
		return ""
	}

	return s
}

func (r *request) StringSlice(name string) []string {
	v, ok := r.Config[name]
	if !ok {
		if r.config != nil {
			return r.config.StringSlice(name)
		}
		return []string{}
	}

	var s []string
	if err := json.Unmarshal(v, &s); err != nil {
		if r.config != nil {
			return r.config.StringSlice(name)
		}
		return []string{}
	}

	return s
}
