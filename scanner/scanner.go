package scanner

import (
	"fmt"
	"net/url"
	"os"
	"sync"

	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/tools"

	log "github.com/Sirupsen/logrus"
)

// Scan is the central dispatcher of the program (basically its actual "main")
func Scan(c config.Config, inlet config.Inlet, outlet config.Outlet) error {
	// Initialize components
	tools.InitHTTP(c)

	// Register all available scanners
	fields, scanners := RegisterScanners(c)
	log.Infof("%d scanners loaded", len(scanners))

	// Print header
	outlet <- (*[]string)(&fields)

	fieldCount := len(fields)
	connections := c.Int("connections")
	if connections <= 0 {
		return fmt.Errorf("Can only use positive number of connections, not %d", connections)
	}

	// jobQueue is filled with all scanner jobs by scanDomain and worked off by the spawned Workers
	jobQueue := make(chan Job, connections)
	workGroup := SpawnWorkers(uint(connections), jobQueue)

	// resolveQueue is used as rate limiting for the domain resolving in scanDomain
	resolveQueue := make(chan int, connections)

	// gwg is the General Wait Group which is used to wait for all domains to finish at the end of the task
	gwg := &sync.WaitGroup{}

	i := 0
	// Load domains from the inlet and pass them to the domain scanner
	for line := range inlet {
		log.Debugf("Got new line from inlet %d: %s", i, line)
		// blocks if the resolve queue is full to leave some breathing space for the actual workers
		resolveQueue <- i

		gwg.Add(1)
		go func(line string, i int) {
			log.Debugf("Started line %d", i)
			defer func(i int) { log.Debugf("done with line %d", i) }(i)
			// Defer the "completed" signals so that no domain is left behind, and no domain leaves work indicators behind
			defer func() { <-resolveQueue }()
			defer gwg.Done()

			ScanDomain(config.ParseHost(line), scanners, fieldCount, jobQueue, outlet, i)
		}(line, i)

		if c.Bool("status") {
			_, _ = fmt.Fprintf(os.Stderr, "%d\n", i)
		}
		i++
	}

	// Wait for the last domains
	gwg.Wait()
	// Close the job queue and therefore kill workers
	close(jobQueue)
	// Wait for all workers to be closed
	workGroup.Wait()

	return nil
}

// ScanDomain resolves all necessary domain data before passing it to the parallel scanners
func ScanDomain(host config.Host, scannerList []config.Scanner, fieldcount int, jobQueue chan Job, outlet config.Outlet, i int) {
	log.Debugf("Started domain scan %d '%s'", i, host.Testing)
	defer func() { log.Debugf("Finished domain %d '%s'\n", i, host.Testing) }()

	// Create the output buffer
	out := make([]string, fieldcount)

	// Resolve the domain to its final destination
	host.URL = GetFinalURL(host.Testing)
	if host.URL != "" {
		host.Online = true

		u, err := url.Parse(host.URL)
		if err != nil {
			log.Debugf("Could not parse host URL '%s': %s", host.URL, err)
			return
		}
		host.Domain = u.Host
	}

	wg := &sync.WaitGroup{}
	wg.Add(len(scannerList))

	used := 0
	// Combine the domain with all scanners to be applied
	for j, scan := range scannerList {
		log.Debugf("Combining domain %d %s with scanner %d %s", i, host.Testing, j, scan.Name)

		jobQueue <- Job{i*100 + j, scan.Scanner, host, out[used : used+len(scan.Fields)], wg}

		used += len(scan.Fields)
		log.Debugf("Combined domain %d with scanner %d", i, j)
	}

	// Wait for all scanners to finish
	wg.Wait()
	// shove the output buffer over to the output function
	log.Debugf("Writing %d to outlet", i)
	outlet <- &out
	log.Debugf("Written %d to outlet", i)
}
