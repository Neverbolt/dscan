package scanners

import (
	"bufio"
	"fmt"
	"gitlab.com/Neverbolt/dscan/scanner"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"unicode"
	"unicode/utf8"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/tools"
)

// Lookup is the parsed struct of singular lookup file entries
type Lookup struct {
	Selector string // Selector can be any css selector that would work with jquery
	Attr     string // Attr is the attribute of the selector that has to contain the string
	String   string // The string to look for
	Fuzzy    bool   // If a fuzzy search should be performed
	Group    string // Group the selector came from (default file stripped of suffix)
}

var (
	// FinderFields are all fields returned by the FinderScanner
	FinderFields = []string{"found"}
	// FinderError is the error that is thrown by FinderScanner
	FinderError = []string{"S"}
	// FinderNone is returned when the site is not online
	FinderNone = []string{""}

	selectorRegex = regexp.MustCompile(`\[([^\]]+)\]`)
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "find",
		Desc: "checks if certain elements and texts are present on the index site",
		Init: FinderInit,
		Flag: config.StringSliceFlag,
	})
}

// FinderInit starts the parsing of all registered finder files
func FinderInit(c config.Config) (config.ScannerFunc, config.ScannerFields, error) {
	files := c.StringSlice("find")

	lookups := make([]Lookup, 0)

	for _, fileName := range files {
		groupTitle := fileName[:len(fileName)-len(filepath.Ext(fileName))]

		file, err := os.Open(fileName)
		if err != nil {
			return nil, nil, fmt.Errorf("Could not load find file '%s': %s", fileName, err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			lookup, err := parseLine(line, &groupTitle)
			if err != nil {
				return nil, nil, fmt.Errorf("Could not parse line '%s' in file '%s': %s", line, fileName, err)
			}
			if lookup != nil {
				lookups = append(lookups, *lookup)
			}
		}

		if err := scanner.Err(); err != nil {
			return nil, nil, err
		}
	}

	wrapperFunc := func(h config.Host) []string {
		if !h.Online {
			return FinderNone
		}
		return finderScanner(h, lookups)
	}

	return wrapperFunc, FinderFields, nil
}

func parseLine(line string, groupTitle *string) (*Lookup, error) {
	lookup := Lookup{Group: *groupTitle}

	if len(line) == 0 {
		return nil, nil
	}

	if line[0] == '{' && line[len(line)-1] == '}' {
		*groupTitle = line[1 : len(line)-1]
		return nil, nil
	}

	var split []string
	if strings.Index(line, "=") > -1 {
		split = strings.Split(line, "=")
	} else if strings.Index(line, "~") > -1 {
		split = strings.Split(line, "~")
		lookup.Fuzzy = true
	} else {
		return nil, fmt.Errorf("No Equals selector")
	}

	if len(split) != 2 {
		return nil, fmt.Errorf("More than one Equals selector")
	}

	lookup.String = strings.TrimSpace(split[1])

	s := selectorRegex.FindAllStringSubmatch(split[0], -1)
	if len(s) != 0 {
		if len(s) > 1 {
			return nil, fmt.Errorf("More than one Field selector")
		}

		if len(s[0]) != 2 {
			return nil, fmt.Errorf("Invalid Field selector: '%s'", s[0][0])
		}

		lookup.Attr = s[0][1]

		lookup.Selector = split[0][:strings.Index(split[0], "[")]

	} else {
		lookup.Selector = split[0]
	}

	return &lookup, nil
}

// finderScanner scans all domains for Lookup entries
func finderScanner(host config.Host, Lookups []Lookup) []string {
	res, err := tools.Request.Get(host.URL)
	if err != nil {
		return FinderError
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return FinderError
	}

	found := make(map[string]bool, 0)
	for _, l := range Lookups {
		if f, ok := found[l.Group]; ok && f {
			continue
		}

		doc.Find(l.Selector).Each(func(i int, s *goquery.Selection) {
			var str string
			if l.Attr != "" {
				if val, ok := s.Attr(l.Attr); ok {
					str = val
				} else {
					return
				}
			} else {
				str = s.Text()
			}

			if l.Fuzzy {
				if fuzzyMatch(l.String, str) {
					found[l.Group] = true
				}
			} else {
				if str == l.String {
					found[l.Group] = true
				}
			}
		})
	}

	foundList := make([]string, 0, len(found))
	for k := range found {
		foundList = append(foundList, k)
	}

	return []string{strings.Join(foundList, config.InnerSeparator)}
}

func fuzzyMatch(sstring, tstring string) bool {
	sstring = strings.TrimSpace(strings.Replace(sstring, "\n", " ", -1))
	tstring = strings.TrimSpace(strings.Replace(tstring, "\n", " ", -1))

	lenDiff := len(tstring) - len(sstring)

	if lenDiff < 0 {
		return false
	}

	if lenDiff == 0 && sstring == tstring {
		return true
	}

	source := []rune(sstring)
	target := []rune(tstring)
	started := false

Outer:
	for i := 0; i < len(source); i++ {
		r1 := source[i]
		for j, r2 := range target {
			if started && j > 8 {
				i = 0
				started = false
				continue Outer
			}
			if unicode.ToLower(r1) == unicode.ToLower(r2) {
				started = true
				target = target[j+utf8.RuneLen(r2):]
				continue Outer
			}
		}

		return false
	}

	return true
}
