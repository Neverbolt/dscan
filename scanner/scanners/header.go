package scanners

import (
	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/scanner"
	"gitlab.com/Neverbolt/dscan/tools"
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "header",
		Desc: "extract http header",
		Init: HeaderInit,
		Flag: config.StringSliceFlag,
	})
}

// HeaderInit looks up all header fields requested and stores them in HeaderFields
func HeaderInit(c config.Config) (config.ScannerFunc, config.ScannerFields, error) {
	headerFields := c.StringSlice("header")
	headerError := make([]string, 0)
	headerNone := make([]string, 0)

	for range headerFields {
		headerError = append(headerError, "E")
		headerNone = append(headerNone, "")
	}

	wrapperFunc := func(h config.Host) []string {
		if !h.Online {
			return headerNone
		}
		return headerScanner(h, headerFields, headerError)
	}

	return wrapperFunc, headerFields, nil
}

// HeaderScanner loads all Header fields that have been requested by the CLI
func headerScanner(host config.Host, headerFields []string, headerError []string) []string {
	resp, err := tools.Request.Get(host.URL)
	if err != nil {
		return headerError
	}

	found := make([]string, len(headerFields))

	for i, field := range headerFields {
		found[i] = resp.Header.Get(field)
	}

	return found
}
