package scanners

import (
	"gitlab.com/Neverbolt/dscan/scanner"
	"strings"

	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/tools"

	log "github.com/Sirupsen/logrus"
)

var (
	// CMSFields is the list of fields that CMSScanner returns
	CMSFields = []string{"cms"}
	// CMSNone is returned when the host is not online
	CMSNone = []string{""}

	cache     = tools.NewCache()

	indicators = map[string][]string{
		"wordpress": {"wp-admin/", "wp-content/", "wp-login.php", "wp-includes/js/wp-lists.js", "wp-content/plugins/akismet/akismet.gif", "wp-content/themes/default/screenshot.png", "wp-images/wpminilogo.png"},
		"joomla":    {"includes/js/dtree/img/frontpage.gif", "images/banners/osmbanner2.png", "media/system/js/mootools.js", "images/joomla_logo_black.jpg", "includes/js/wz_tooltip.js", "includes/js/tabs/tabpane_mini.js"},
		"typo3":     {"fileadmin/content/01_about_this_page/usetypo3-logo-preview.png", "typo3/", "typo3temp/"},
		"phpbb":     {"images/avatars/gallery/index.htm", "adm/style/permission_trace.html", "images/smilies/icon_e_confused.gif"},
		"drupal":    {"misc/drupal.js", "themes/chameleon/marvin/bullet.png", "themes/pushbutton/arrow-up-visited.png", "misc/throbber.gif", "misc/watchdog-error.png"},
	}
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "cms",
		Desc: "scan for cms by directory \"fuzzing\"",
		Scanner: CMSScanner,
		Fields: CMSFields,
	})
}

// CMSScanner scans the websites for predefined CMSs
func CMSScanner(host config.Host) []string {
	if !host.Online {
		return CMSNone
	}

	if res := scanCMSAt(host.URL, host); res != nil {
		return []string{*res}
	}

	return []string{"other"}
}

func scanCMSAt(url string, host config.Host) *string {
	if !strings.HasSuffix(url, "/") {
		url = url + "/"
	}

	if c, loaded := cache.LoadOrReserve(url); loaded {
		log.Debugf("Loaded from cache %s: %v\n", url, c)
		if c != nil {
			return c.(*string)
		}
	}

	detected := make([]string, 0, 1)
	for cms, links := range indicators {
		found := 0
		nlinks := len(links)
		for i, link := range links {
			resp, err := tools.Request.Get(url + link)
			if err != nil {
				log.Debugf("err: %s\n", err)
				continue
			}
			defer resp.Body.Close()

			sc := resp.StatusCode
			if sc == 401 || sc == 403 || (200 <= sc && sc < 300) {
				found++
				if found > nlinks/3 {
					detected = append(detected, cms)
					break
				}
			} else {
				if (nlinks/3 - found) > (nlinks - i) {
					break
				}
			}
		}
	}

	if len(detected) == 1 {
		cache.Store(url, &detected[0])
		return &detected[0]
	} else if len(detected) > 1 {
		tmp := "E"
		cache.Store(url, &tmp)
		return &tmp
	}

	cache.Store(url, nil)
	return nil
}
