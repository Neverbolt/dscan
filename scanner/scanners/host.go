package scanners

import (
	"gitlab.com/Neverbolt/dscan/scanner"
	"net"

	"gitlab.com/Neverbolt/dscan/config"
)

var (
	// HostFields are all fields returned by HostScan
	HostFields = []string{"ip", "hoster"}
	// HostError is the error that HostScan can return
	HostError = []string{"E", "E"}
	// HostNone is returned when the host is offline
	HostNone = []string{"", ""}
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "host",
		Desc: "resolve the original hoster (aws, gcloud, ...)",
		Scanner: HostScan,
		Fields: HostFields,
	})
}

// HostScan does a reverse IP lookup to find the original hoster of a website
func HostScan(host config.Host) []string {
	if !host.Online {
		return HostNone
	}

	addrs, err := net.LookupHost(host.Domain)
	if err != nil {
		return HostError
	}

	for _, addr := range addrs {
		hoster, err := net.LookupAddr(addr)
		if err == nil {
			return []string{addr, hoster[0]}
		}
	}

	if len(addrs) > 0 {
		return []string{addrs[0], "E"}
	}

	return HostError
}
