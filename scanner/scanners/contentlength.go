package scanners

import (
	"gitlab.com/Neverbolt/dscan/scanner"
	"io"
	"io/ioutil"
	"strconv"

	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/tools"
)

var (
	// ContentLengthFields are all fields returned by ContentLengthScan
	ContentLengthFields = []string{"Content-Length"}
	// ContentLengthError is the error that ContentLengthScanner can return
	ContentLengthError = []string{"E"}
	// ContentLengthNone is returned when the host is not online
	ContentLengthNone = []string{""}
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "content-length",
		Desc: "returns the content length of the HTTP response, since golang does not show it as Header for the header scanner",
		Scanner: ContentLengthScanner,
		Fields: ContentLengthFields,
		Flag: config.BoolFlag,
	})
}

// ContentLengthScanner does a reverse IP lookup to find the original hoster of a website
func ContentLengthScanner(host config.Host) []string {
	if !host.Online {
		return ContentLengthNone
	}

	resp, err := tools.Request.Get(host.URL)
	if err != nil {
		return ContentLengthError
	}

	b, err := io.Copy(ioutil.Discard, resp.Body)
	if err != nil {
		return ContentLengthError
	}

	return []string{strconv.FormatInt(b, 10)}
}
