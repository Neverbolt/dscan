package scanners

import (
	"fmt"
	"gitlab.com/Neverbolt/dscan/scanner"
	"net"
	"strconv"
	"strings"

	"github.com/miekg/dns"

	"gitlab.com/Neverbolt/dscan/config"
)

var (
	// DNSFields contain all dns fields that are requested in the cli
	DNSFields = []string{}
	// DNSError contains "E"s for all dns fields that are requested in the cli
	DNSError = []string{}
	// DNSNone contains empty strings for all dns fields that are requested in the cli
	DNSNone = []string{}
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "dns",
		Desc: "load dns entriy for final url",
		Init: DNSInit,
		Flag: config.StringSliceFlag,
	})
}

// DNSInit reads all requested fields into the DNSFields list
func DNSInit(c config.Config) (config.ScannerFunc, config.ScannerFields, error) {
	DNSFields = c.StringSlice("dns")

	s = net.JoinHostPort(c.String("resolver"), "53")

	for _, field := range DNSFields {
		if _, ok := nstypes[field]; !ok {
			return nil, nil, fmt.Errorf("Not a valid nslookup type '%s'", field)
		}
		DNSError = append(DNSError, "E")
		DNSNone = append(DNSNone, "")
	}

	return DNSScan, DNSFields, nil
}

// DNSScan scans all requested fields for a given host
func DNSScan(host config.Host) []string {
	if !host.Online {
		return DNSNone
	}

	res := make([]string, len(DNSFields))

	for i, t := range DNSFields {
		m := new(dns.Msg)
		m.SetQuestion(dns.Fqdn(host.Testing), nstypes[t].ID)
		m.RecursionDesired = true

		r, _, err := c.Exchange(m, s)
		if err != nil || r == nil || r.Rcode != dns.RcodeSuccess {
			res[i] = "E"
			continue
		}

		s := ""

		for _, rr := range r.Answer {
			if rr.Header().Rrtype == nstypes[t].ID {
				if len(s) != 0 {
					s += config.InnerSeparator
				}

				s += nstypes[t].String(rr)
			}
		}

		res[i] = s
	}

	return res
}

type nstype struct {
	ID     uint16
	String func(dns.RR) string
}

func cmToM(m, e uint8) string {
	if e < 2 {
		if e == 1 {
			m *= 10
		}

		return fmt.Sprintf("0.%02d", m)
	}

	s := fmt.Sprintf("%d", m)
	for e > 2 {
		s += "0"
		e--
	}
	return s
}

func saltToString(s string) string {
	if len(s) == 0 {
		return "-"
	}
	return strings.ToUpper(s)
}

func isDigit(b byte) bool { return b >= '0' && b <= '9' }

func dddToByte(s []byte) byte {
	return byte((s[0]-'0')*100 + (s[1]-'0')*10 + (s[2] - '0'))
}

func nextByte(b []byte, offset int) (byte, int) {
	if offset >= len(b) {
		return 0, 0
	}
	if b[offset] != '\\' {
		// not an escape sequence
		return b[offset], 1
	}
	switch len(b) - offset {
	case 1: // dangling escape
		return 0, 0
	case 2, 3: // too short to be \ddd
	default: // maybe \ddd
		if isDigit(b[offset+1]) && isDigit(b[offset+2]) && isDigit(b[offset+3]) {
			return dddToByte(b[offset+1:]), 4
		}
	}
	// not \ddd, just an RFC 1035 "quoted" character
	return b[offset+1], 2
}

func appendByte(s []byte, b byte) []byte {
	var buf [3]byte
	bufs := strconv.AppendInt(buf[:0], int64(b), 10)
	s = append(s, '\\')
	for i := 0; i < 3-len(bufs); i++ {
		s = append(s, '0')
	}
	for _, r := range bufs {
		s = append(s, r)
	}
	return s
}

func sprintTxtOctet(s string) string {
	src := []byte(s)
	dst := make([]byte, 0, len(src))
	dst = append(dst, '"')
	for i := 0; i < len(src); {
		if i+1 < len(src) && src[i] == '\\' && src[i+1] == '.' {
			dst = append(dst, src[i:i+2]...)
			i += 2
		} else {
			b, n := nextByte(src, i)
			if n == 0 {
				i++ // dangling back slash
			} else if b == '.' {
				dst = append(dst, b)
			} else {
				if b < ' ' || b > '~' {
					dst = appendByte(dst, b)
				} else {
					dst = append(dst, b)
				}
			}
			i += n
		}
	}
	dst = append(dst, '"')
	return string(dst)
}

var (
	nstypes = map[string]nstype{
		"A": {
			dns.TypeA,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.A); ok {
					return v.A.String()
				}
				return "E"
			},
		},
		"NS": {
			dns.TypeNS,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NS); ok {
					return v.Ns
				}
				return "E"
			},
		},
		"MD": {
			dns.TypeMD,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.MD); ok {
					return v.Md
				}
				return "E"
			},
		},
		"MF": {
			dns.TypeMF,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.MF); ok {
					return v.Mf
				}
				return "E"
			},
		},
		"CNAME": {
			dns.TypeCNAME,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.CNAME); ok {
					return v.Target
				}
				return "E"
			},
		},
		"SOA": {
			dns.TypeSOA,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.SOA); ok {
					return strings.Join([]string{"\"" + v.Ns + "\"", "\"" + v.Mbox + "\"",
						strconv.FormatInt(int64(v.Serial), 10), strconv.FormatInt(int64(v.Refresh), 10), strconv.FormatInt(int64(v.Retry), 10),
						strconv.FormatInt(int64(v.Expire), 10), strconv.FormatInt(int64(v.Minttl), 10)},
						" ")
				}
				return "E"
			},
		},
		"MB": {
			dns.TypeMB,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.MB); ok {
					return v.Mb
				}
				return "E"
			},
		},
		"MG": {
			dns.TypeMG,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.MG); ok {
					return v.Mg
				}
				return "E"
			},
		},
		"MR": {
			dns.TypeMR,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.MR); ok {
					return v.Mr
				}
				return "E"
			},
		},
		"PTR": {
			dns.TypePTR,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.PTR); ok {
					return v.Ptr
				}
				return "E"
			},
		},
		"HINFO": {
			dns.TypeHINFO,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.HINFO); ok {
					return fmt.Sprintf("\"%s\" \"%s\"", v.Cpu, v.Os)
				}
				return "E"
			},
		},
		"MINFO": {
			dns.TypeMINFO,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.MINFO); ok {
					return fmt.Sprintf("\"%s\" \"%s\"", v.Rmail, v.Email)
				}
				return "E"
			},
		},
		"MX": {
			dns.TypeMX,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.MX); ok {
					return fmt.Sprintf("%d \"%s\"", v.Preference, v.Mx)
				}
				return "E"
			},
		},
		"TXT": {
			dns.TypeTXT,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.TXT); ok {
					return strings.Join(v.Txt, "\n")
				}
				return "E"
			},
		},
		"RP": {
			dns.TypeRP,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.RP); ok {
					return fmt.Sprintf("%s \"%s\"", v.Mbox, v.Txt)
				}
				return "E"
			},
		},
		"AFSDB": {
			dns.TypeAFSDB,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.AFSDB); ok {
					return fmt.Sprintf("%d \"%s\"", v.Subtype, v.Hostname)
				}
				return "E"
			},
		},
		"X25": {
			dns.TypeX25,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.X25); ok {
					return v.PSDNAddress
				}
				return "E"
			},
		},
		"RT": {
			dns.TypeRT,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.RT); ok {
					return fmt.Sprintf("%d \"%s\"", v.Preference, v.Host)
				}
				return "E"
			},
		},
		"NSAPPTR": {
			dns.TypeNSAPPTR,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NSAPPTR); ok {
					return v.Ptr
				}
				return "E"
			},
		},
		"SIG": {
			dns.TypeSIG,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.SIG); ok {
					return strings.Join([]string{
						dns.Type(v.TypeCovered).String(),
						strconv.Itoa(int(v.Algorithm)),
						strconv.Itoa(int(v.Labels)),
						strconv.FormatInt(int64(v.OrigTtl), 10),
						dns.TimeToString(v.Expiration),
						dns.TimeToString(v.Inception),
						strconv.Itoa(int(v.KeyTag)),
						"\"" + v.SignerName + "\"",
						v.Signature}, " ")
				}
				return "E"
			},
		},
		"KEY": {
			dns.TypeKEY,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.KEY); ok {
					return strings.Join([]string{
						strconv.Itoa(int(v.Flags)),
						strconv.Itoa(int(v.Protocol)),
						strconv.Itoa(int(v.Algorithm)),
						v.PublicKey}, " ")
				}
				return "E"
			},
		},
		"PX": {
			dns.TypePX,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.PX); ok {
					return fmt.Sprintf("%d \"%s\" \"%s\"", v.Preference, v.Map822, v.Mapx400)
				}
				return "E"
			},
		},
		"GPOS": {
			dns.TypeGPOS,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.GPOS); ok {
					return strings.Join([]string{v.Longitude, v.Latitude, v.Altitude}, " ")
				}
				return "E"
			},
		},
		"AAAA": {
			dns.TypeAAAA,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.AAAA); ok {
					return v.AAAA.String()
				}
				return "E"
			},
		},
		"LOC": {
			dns.TypeLOC,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.LOC); ok {
					lat := v.Latitude
					ns := "N"
					if lat > dns.LOC_EQUATOR {
						lat = lat - dns.LOC_EQUATOR
					} else {
						ns = "S"
						lat = dns.LOC_EQUATOR - lat
					}
					h := lat / dns.LOC_DEGREES
					lat = lat % dns.LOC_DEGREES
					m := lat / dns.LOC_HOURS
					lat = lat % dns.LOC_HOURS
					s := fmt.Sprintf("%02d %02d %0.3f %s ", h, m, (float64(lat) / 1000), ns)

					lon := v.Longitude
					ew := "E"
					if lon > dns.LOC_PRIMEMERIDIAN {
						lon = lon - dns.LOC_PRIMEMERIDIAN
					} else {
						ew = "W"
						lon = dns.LOC_PRIMEMERIDIAN - lon
					}
					h = lon / dns.LOC_DEGREES
					lon = lon % dns.LOC_DEGREES
					m = lon / dns.LOC_HOURS
					lon = lon % dns.LOC_HOURS
					s += fmt.Sprintf("%02d %02d %0.3f %s ", h, m, (float64(lon) / 1000), ew)

					var alt = float64(v.Altitude) / 100
					alt -= dns.LOC_ALTITUDEBASE
					if v.Altitude%100 != 0 {
						s += fmt.Sprintf("%.2fm ", alt)
					} else {
						s += fmt.Sprintf("%.0fm ", alt)
					}

					s += cmToM((v.Size&0xf0)>>4, v.Size&0x0f) + "m "
					s += cmToM((v.HorizPre&0xf0)>>4, v.HorizPre&0x0f) + "m "
					s += cmToM((v.VertPre&0xf0)>>4, v.VertPre&0x0f) + "m"
					return s
				}
				return "E"
			},
		},
		"EID": {
			dns.TypeEID,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.EID); ok {
					return v.Endpoint
				}
				return "E"
			},
		},
		"NIMLOC": {
			dns.TypeNIMLOC,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NIMLOC); ok {
					return v.Locator
				}
				return "E"
			},
		},
		"SRV": {
			dns.TypeSRV,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.SRV); ok {
					return fmt.Sprintf("%d %d %d \"%s\"", v.Priority, v.Weight, v.Port, v.Target)
				}
				return "E"
			},
		},
		"NAPTR": {
			dns.TypeNAPTR,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NAPTR); ok {
					return fmt.Sprintf("%d %d \"%s\" \"%s\" \"%s\" %s", v.Order, v.Preference, v.Flags, v.Service, v.Regexp, v.Replacement)
				}
				return "E"
			},
		},
		"KX": {
			dns.TypeKX,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.KX); ok {
					return fmt.Sprintf("%d \"%s\"", v.Preference, v.Exchanger)
				}
				return "E"
			},
		},
		"CERT": {
			dns.TypeCERT,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.CERT); ok {
					var (
						ok                  bool
						certtype, algorithm string
					)
					if certtype, ok = dns.CertTypeToString[v.Type]; !ok {
						certtype = strconv.Itoa(int(v.Type))
					}
					if algorithm, ok = dns.AlgorithmToString[v.Algorithm]; !ok {
						algorithm = strconv.Itoa(int(v.Algorithm))
					}
					return strings.Join([]string{certtype, strconv.Itoa(int(v.KeyTag)), algorithm, v.Certificate}, " ")
				}
				return "E"
			},
		},
		"DNAME": {
			dns.TypeDNAME,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.DNAME); ok {
					return v.Target
				}
				return "E"
			},
		},
		"DS": {
			dns.TypeDS,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.DS); ok {
					return strings.Join([]string{
						strconv.Itoa(int(v.KeyTag)), strconv.Itoa(int(v.Algorithm)), strconv.Itoa(int(v.DigestType)), strings.ToUpper(v.Digest),
					}, " ")
				}
				return "E"
			},
		},
		"SSHFP": {
			dns.TypeSSHFP,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.SSHFP); ok {
					return strings.Join([]string{strconv.Itoa(int(v.Algorithm)), strconv.Itoa(int(v.Type)), strings.ToUpper(v.FingerPrint)}, " ")
				}
				return "E"
			},
		},
		"RRSIG": {
			dns.TypeRRSIG,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.RRSIG); ok {
					return strings.Join([]string{
						dns.Type(v.TypeCovered).String(),
						strconv.Itoa(int(v.Algorithm)),
						strconv.Itoa(int(v.Labels)),
						strconv.FormatInt(int64(v.OrigTtl), 10),
						dns.TimeToString(v.Expiration),
						dns.TimeToString(v.Inception),
						strconv.Itoa(int(v.KeyTag)),
						"\"" + v.SignerName + "\"",
						v.Signature,
					}, " ")
				}
				return "E"
			},
		},
		"NSEC": {
			dns.TypeNSEC,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NSEC); ok {
					s := v.NextDomain
					for i := 0; i < len(v.TypeBitMap); i++ {
						s += " " + dns.Type(v.TypeBitMap[i]).String()
					}
					return s
				}
				return "E"
			},
		},
		"DNSKEY": {
			dns.TypeDNSKEY,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.DNSKEY); ok {
					return strings.Join([]string{
						strconv.Itoa(int(v.Flags)),
						strconv.Itoa(int(v.Protocol)),
						strconv.Itoa(int(v.Algorithm)),
						v.PublicKey}, " ")
				}
				return "E"
			},
		},
		"DHCID": {
			dns.TypeDHCID,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.DHCID); ok {
					return v.Digest
				}
				return "E"
			},
		},
		"NSEC3": {
			dns.TypeNSEC3,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NSEC3); ok {
					s := strconv.Itoa(int(v.Hash)) +
						" " + strconv.Itoa(int(v.Flags)) +
						" " + strconv.Itoa(int(v.Iterations)) +
						" " + saltToString(v.Salt) +
						" " + v.NextDomain

					for i := 0; i < len(v.TypeBitMap); i++ {
						s += " " + dns.Type(v.TypeBitMap[i]).String()
					}
					return s
				}
				return "E"
			},
		},
		"NSEC3PARAM": {
			dns.TypeNSEC3PARAM,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NSEC3PARAM); ok {
					return strconv.Itoa(int(v.Hash)) +
						" " + strconv.Itoa(int(v.Flags)) +
						" " + strconv.Itoa(int(v.Iterations)) +
						" " + saltToString(v.Salt)
				}
				return "E"
			},
		},
		"TLSA": {
			dns.TypeTLSA,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.TLSA); ok {
					return strconv.Itoa(int(v.Usage)) +
						" " + strconv.Itoa(int(v.Selector)) +
						" " + strconv.Itoa(int(v.MatchingType)) +
						" " + v.Certificate
				}
				return "E"
			},
		},
		"SMIMEA": {
			dns.TypeSMIMEA,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.SMIMEA); ok {
					return strconv.Itoa(int(v.Usage)) +
						" " + strconv.Itoa(int(v.Selector)) +
						" " + strconv.Itoa(int(v.MatchingType)) +
						v.Certificate
				}
				return "E"
			},
		},
		"HIP": {
			dns.TypeHIP,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.HIP); ok {
					s := strconv.Itoa(int(v.PublicKeyAlgorithm)) +
						" " + v.Hit +
						" " + v.PublicKey

					for _, d := range v.RendezvousServers {
						s += " \"" + d + "\""
					}

					return s
				}
				return "E"
			},
		},
		"NINFO": {
			dns.TypeNINFO,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NINFO); ok {
					return strings.Join(v.ZSData, " ")
				}
				return "E"
			},
		},
		"RKEY": {
			dns.TypeRKEY,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.RKEY); ok {
					return strconv.Itoa(int(v.Flags)) +
						" " + strconv.Itoa(int(v.Protocol)) +
						" " + strconv.Itoa(int(v.Algorithm)) +
						" " + v.PublicKey
				}
				return "E"
			},
		},
		"TALINK": {
			dns.TypeTALINK,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.TALINK); ok {
					return fmt.Sprintf("\"%s\" \"%s\"", v.PreviousName, v.NextName)
				}
				return "E"
			},
		},
		"CDS": {
			dns.TypeCDS,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.CDS); ok {
					return strconv.Itoa(int(v.KeyTag)) +
						" " + strconv.Itoa(int(v.Algorithm)) +
						" " + strconv.Itoa(int(v.DigestType)) +
						" " + strings.ToUpper(v.Digest)
				}
				return "E"
			},
		},
		"CDNSKEY": {
			dns.TypeCDNSKEY,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.CDNSKEY); ok {
					return strconv.Itoa(int(v.Flags)) +
						" " + strconv.Itoa(int(v.Protocol)) +
						" " + strconv.Itoa(int(v.Algorithm)) +
						" " + v.PublicKey
				}
				return "E"
			},
		},
		"OPENPGPKEY": {
			dns.TypeOPENPGPKEY,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.OPENPGPKEY); ok {
					return v.PublicKey
				}
				return "E"
			},
		},
		"CSYNC": {
			dns.TypeCSYNC,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.CSYNC); ok {
					s := strconv.FormatInt(int64(v.Serial), 10) + " " + strconv.Itoa(int(v.Flags))

					for i := 0; i < len(v.TypeBitMap); i++ {
						s += " " + dns.Type(v.TypeBitMap[i]).String()
					}

					return s
				}
				return "E"
			},
		},
		"SPF": {
			dns.TypeSPF,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.SPF); ok {
					return strings.Join(v.Txt, " ")
				}
				return "E"
			},
		},
		"UINFO": {
			dns.TypeUINFO,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.UINFO); ok {
					return v.Uinfo
				}
				return "E"
			},
		},
		"UID": {
			dns.TypeUID,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.UID); ok {
					return strconv.Itoa(int(v.Uid))
				}
				return "E"
			},
		},
		"GID": {
			dns.TypeGID,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.GID); ok {
					return strconv.Itoa(int(v.Gid))
				}
				return "E"
			},
		},
		"NID": {
			dns.TypeNID,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.NID); ok {
					node := fmt.Sprintf("%0.16x", v.NodeID)
					return strconv.Itoa(int(v.Preference)) + " " + node[0:4] + ":" + node[4:8] + ":" + node[8:12] + ":" + node[12:16]
				}
				return "E"
			},
		},
		"L32": {
			dns.TypeL32,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.L32); ok {
					if v.Locator32 == nil {
						return strconv.Itoa(int(v.Preference))
					}

					return strconv.Itoa(int(v.Preference)) + " " + v.Locator32.String()
				}
				return "E"
			},
		},
		"L64": {
			dns.TypeL64,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.L64); ok {
					node := fmt.Sprintf("%0.16X", v.Locator64)
					return strconv.Itoa(int(v.Preference)) + " " + node[0:4] + ":" + node[4:8] + ":" + node[8:12] + ":" + node[12:16]
				}
				return "E"
			},
		},
		"LP": {
			dns.TypeLP,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.LP); ok {
					return strconv.Itoa(int(v.Preference)) + " \"" + v.Fqdn + "\""
				}
				return "E"
			},
		},
		"EUI48": {
			dns.TypeEUI48,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.EUI48); ok {
					return strconv.Itoa(int(v.Address))
				}
				return "E"
			},
		},
		"EUI64": {
			dns.TypeEUI64,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.EUI64); ok {
					return strconv.Itoa(int(v.Address))
				}
				return "E"
			},
		},
		"URI": {
			dns.TypeURI,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.URI); ok {
					return strconv.Itoa(int(v.Priority)) + " " + strconv.Itoa(int(v.Weight)) + " " + sprintTxtOctet(v.Target)
				}
				return "E"
			},
		},
		"CAA": {
			dns.TypeCAA,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.CAA); ok {
					return strconv.Itoa(int(v.Flag)) + " " + v.Tag + " " + sprintTxtOctet(v.Value)
				}
				return "E"
			},
		},
		"AVC": {
			dns.TypeAVC,
			func(rr dns.RR) string {
				if v, ok := rr.(*dns.AVC); ok {
					return strings.Join(v.Txt, " ")
				}
				return "E"
			},
		},
	}

	s = net.JoinHostPort("8.8.8.8", "53") // use Google DNS as default
	c = new(dns.Client)
)
