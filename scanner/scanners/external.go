package scanners

import (
	"fmt"
	"gitlab.com/Neverbolt/dscan/scanner"
	"os/exec"
	"path"
	"strings"

	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/tools"

	log "github.com/Sirupsen/logrus"
)

var (
	replacer = strings.NewReplacer("\"", "\\\"")
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "run",
		Desc: "runs external scripts that are passed DOMAIN and URL as parameter (ignores stderr)",
		Init: ExternalInit,
		Flag: config.StringSliceFlag,
	})
}

// ExternalInit stats all scripts and saves their location
func ExternalInit(c config.Config) (config.ScannerFunc, config.ScannerFields, error) {
	externalFields := make([]string, 0)

	scripts := c.StringSlice("run")
	scriptLocations := make([]string, 0, len(scripts))
	configdir := c.String("configdir")

	for _, script := range scripts {
		loc, err := tools.GetScriptLocation(script, configdir)
		if err != nil {
			return nil, nil, fmt.Errorf("Could not find script file '%s': %s", script, err)
		}
		scriptLocations = append(scriptLocations, loc)

		externalFields = append(externalFields, path.Base(script))
	}

	wrapperFunc := func(h config.Host) []string {
		return externalExecute(h, scriptLocations)
	}

	return wrapperFunc, externalFields, nil
}

// externalExecute executes an external script by calling it with `sh`
func externalExecute(host config.Host, scripts []string) []string {
	results := make([]string, len(scripts))

	for i, script := range scripts {
		output, err := exec.Command(script, host.Testing, host.Domain, host.URL).Output()
		if err != nil {
			log.Errorf("Could not start script %s: %s", script, err)
		}

		results[i] = "\"" + replacer.Replace(strings.TrimSuffix(string(output), "\n")) + "\""
	}

	return results
}
