package scanners

import (
	"crypto/tls"
	"io/ioutil"
	"net"
	"net/http"
	"regexp"
	"sort"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/miekg/dns"
	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/scanner"
	"gitlab.com/Neverbolt/dscan/tools"
)

type PublicProvider struct {
	Cname                     []string
	CnameRegex                string
	SourceDnsEntryRegex       string
	Response                  []string
	HttpStatusCode            int
	HttpServerHeaderMustMatch string
	HttpLocationHeader        []string
	SkipHttpCheck             bool
	CnameForVerifyOnly        bool
}

var (
	DnsRequestTimeout = 10 * time.Second
	DnsResolver       = net.JoinHostPort("8.8.8.8", "53")
	DnsClient         = new(dns.Client)
	DetailedResults   = false
	// value of provider: ["UNKNOWN" or "<Name of provider>"]
	// value of takeover_possible: ["YES", "NO", "MAYBE"]
	SubdomainTakeoverFields         = []string{"provider", "takeover_possible"}
	SubdomainTakeoverDetailedFields = []string{"error", "provider", "lookup_method", "takeover_possible"}
	SubdomainTakeoverErrorHTTP      = "ERROR-CANNOT-READ-HTTP-CONTENT"
	SubdomainTakeoverErrorReadBody  = "ERROR-CANNOT-READ-HTTP-BODY"
	SubdomainTakeoverErrorRegex     = "ERROR-PROBLEM-WITH-REGEX"
	Providers                       = map[string]PublicProvider{
		"skip-SIP": {
			SourceDnsEntryRegex: "_(h323ls|h323cs|collab-edge|sipfederationtls|sips?)\\._(tcp|udp|tls)\\.",
			CnameForVerifyOnly:  true, // skipping sip entries
		},
		"acquia.com": {
			Cname:    []string{"acquia.com"},
			Response: []string{"If you are an Acquia Cloud customer and expect to see your site at this address"},
		},
		"acquia-test.co": {
			Cname:    []string{"acquia-test.co"},
			Response: []string{"The site you are looking for could not be found."},
		},
		"activecampaign.com": {
			Cname:    []string{"activehosted.com"},
			Response: []string{"alt=\"LIGHTTPD - fly light.\""},
		},
		"aftership.com": {
			Cname:    []string{"aftership.com"},
			Response: []string{"Oops.</h2><p class=\"text-muted text-tight\">The page you're looking for doesn't exist."},
		},
		"aha.io": {
			Cname:    []string{"ideas.aha.io"},
			Response: []string{"There is no portal here ... sending you back to Aha!"},
		},
		"akamai.com": {
			Cname: []string{".akamaiedge.net", ".dscr.akamai.net", ".akamaized.net", ".akamaistream.net",
				".edgesuite.net", ".edgekey.net", ".edgekey.com", ".akamaihd.net", ".edgefcs.net"},
			Response: []string{"<H1>Invalid URL</H1>"},
		},
		"akamai.com-Staging": {
			Cname:    []string{".edgekey-staging.net"},
			Response: []string{"<H1>Invalid URL</H1>"},
		},
		"amazonaws.com-ELB": {
			CnameRegex:                "\\.elb\\.([^\\.]*\\.)?amazonaws\\.com",
			HttpStatusCode:            404,
			HttpServerHeaderMustMatch: "awselb/2.0",
		},
		"amazonaws.com-S3": {
			CnameRegex: "(s3|s3-[^\\.]+)\\.amazonaws\\.com",
			Response:   []string{"NoSuchBucket", "The specified bucket does not exist"},
		},
		"amazonaws.com-SES": {
			Cname:              []string{".amazonses.com", ".awstrack.me"},
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"amazonaws.com-Compute": {
			Cname: []string{".compute.amazonaws.com"},
		},
		"amazonaws.com-Certificate-Manager": {
			Cname:              []string{".acm-validations.aws"},
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"amazonaws.com-X-Service": {
			Cname: []string{".amazonaws.com"}, // generic catchall for all AWS services not defined before
		},
		"auth0.com": {
			Cname: []string{".auth0.com"},
		},
		"azure.net": {
			Cname: []string{".azurewebsites.net", ".azureedge.net", ".cloudapp.net"},
			Response: []string{"404 Web Site not found", "Our services aren't available right now",
				"HTTP Error 404. The requested resource is not found."},
		},
		"barracuda.com": {
			Cname:              []string{"ess.barracuda.com"},
			SkipHttpCheck:      true, // domain is only used for validation
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"bettywebblocks.com": {
			Cname:    []string{".bettywebblocks.com"},
			Response: []string{"THIS APPLICATION GOT LOST IN HYPERSPACE"},
		},
		"bevy.com": {
			Cname: []string{".bevylabs.com"},
		},
		"bigcartel.com": {
			Cname:    []string{"bigcartel.com"},
			Response: []string{"<h1>Oops! We couldn&#8217;t find that page.</h1>"},
		},
		"bing.com": {
			Cname: []string{"verify.bing.com"},
		},
		"bitbucket.org": {
			Cname:    []string{"bitbucket.org"},
			Response: []string{"The page you have requested does not exist"},
		},
		"bitly.com": {
			Cname:              []string{"cname.bit.ly", "cname.bitly.com"},
			CnameForVerifyOnly: true, // cname to cname.bitly.com is only used for domain validation
		},
		"brightcove.com": {
			Cname:    []string{"brightcovegallery.com", "gallery.video", "bcvp0rtal.com"},
			Response: []string{"<p class=\"bc-gallery-error-code\">Error Code: 404</p>"},
		},
		"bynder.com": {
			Cname: []string{".getbynder.com"},
		},
		"campaignmonitor.com": {
			Cname:    []string{"createsend.com", "name.createsend.com"},
			Response: []string{"Double check the URL", "<strong>Trying to access your account?</strong>"},
		},
		"cargo.site": {
			Cname:    []string{"cargocollective.com"},
			Response: []string{"If you're moving your domain away from Cargo you must make this configuration through your registrar's DNS control panel.", "404 Not Found"},
		},
		"ceros.com": {
			Cname:              []string{"view.ceros.com"},
			HttpLocationHeader: []string{"www.ceros.com"},
			HttpStatusCode:     301, //They redirect to their page if the domain does not exists
		},
		"cleverreach.com": {
			Cname: []string{".cleverreach.com"},
		},
		"cloudfront.com": {
			Cname:    []string{"cloudfront.net"},
			Response: []string{"The request could not be satisfied", "ERROR: The request could not be satisfied"},
		},
		"cloudinary.com": {
			Cname:                     []string{".cloudinary.com"},
			HttpServerHeaderMustMatch: "Cloudinary",
			HttpStatusCode:            404,
		},
		"comodoca.com": {
			Cname:              []string{".comodoca.com"},
			SkipHttpCheck:      true, // only used for domain validation
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"compaignmonitor": {
			Cname:    []string{"createsend.com"},
			Response: []string{"Double check the URL or <a href=\"mailto:help@createsend.com"},
		},
		"cotendo.com (Akamai)": {
			Cname: []string{".cotcdn.net"},
		},
		"crowdin.com": {
			Cname:              []string{"cname.crowdin.com"},
			HttpLocationHeader: []string{"//crowdin.com"}, // if page is not present it redirects to their main page
		},
		"desk.com": {
			Cname:    []string{".desk.com"},
			Response: []string{"Please try again or try Desk.com free for 14 days.", "Sorry, We Couldn't Find That Page"},
		},
		"divio.com": {
			Cname:    []string{"aldryn-media.io", "aldryn.io"},
			Response: []string{"Application not responding"},
		},
		"egnyte.com": {
			Cname:    []string{".egnyte.com"},
			Response: []string{"404 File Not Found", "The webpage that you have requested was not found."},
		},
		"emarsys.com": {
			Cname: []string{".emarsys.net", ".ems-ci.com", ".emarsys-is.de"},
		},
		"fastly.net": {
			Cname:    []string{"fastly.net"},
			Response: []string{"Please check that this domain has been added to a service", "Fastly error: unknown domain"},
		},
		"feedburner.com": {
			Cname:    []string{"feeds.feedburner.com"},
			Response: []string{"404."},
		},
		"feedpress.com": {
			Cname:    []string{"redirect.feedpress.me"},
			Response: []string{"The feed has not been found."},
		},
		"firebase.com": {
			Cname:    []string{".firebaseapp.com"},
			Response: []string{"<h1>Site Not Found</h1>"},
		},
		"cloud.google.com-Bucket": {
			Cname:    []string{"googleapis.com"},
			Response: []string{"<Error><Code>NoSuchBucket</Code>"},
		},
		"getresponse.com": {
			Cname:    []string{".gr8.com"},
			Response: []string{"With GetResponse Landing Pages, lead generation has never been easier"},
		},
		"ghost.org": {
			Cname:    []string{"ghost.io"},
			Response: []string{"The thing you were looking for is no longer here", "The thing you were looking for is no longer here, or never was"},
		},
		"gigya.com": {
			Cname: []string{".gigya.com", ".gigya-api.com"},
		},
		"github.com": {
			Cname:    []string{"github.io", "github.map.fastly.net"},
			Response: []string{"There isn't a GitHub Pages site here.", "For root URLs (like http://example.com/) you must provide an index.html file"},
		},
		"gitlab.com": {
			Cname:    []string{"namespace.gitlab.io"},
			Response: []string{"<a href=\"https://projects.gitlab.io/auth?domain="},
		},
		"gsuite.google.com": {
			Cname: []string{".domainverify.googlehosted.com", ".dv.googlehosted.com", "ghs.google.com",
				"ghs.googlehosted.com", "ghs4.googlehosted.com", "ghs6.googlehosted.com", "ghs46.googlehosted.com"},
			SkipHttpCheck:      true,
			CnameForVerifyOnly: true, // only for verification of domain ownership
		},
		"helpjuice.com": {
			Cname:    []string{"helpjuice.com"},
			Response: []string{"We could not find what you're looking for."},
		},
		"helpscout.com": {
			Cname:    []string{"helpscoutdocs.com"},
			Response: []string{"No settings were found for this company:"},
		},
		"heroku.com": {
			Cname:    []string{"herokudns.com", "herokussl.com", "herokuapp.com"},
			Response: []string{"There's nothing here, yet.", "herokucdn.com/error-pages/no-such-app.html", "<title>No such app</title>"},
		},
		"hubspot.net": {
			Cname:    []string{".ites.hubspot.net"},
			Response: []string{"Our website provider is having trouble loading this page"},
		},
		"humanmade.com": {
			Cname: []string{".hmn.md"},
		},
		"instapage.com": {
			Cname:    []string{"pageserve.co", "secure.pageserve.co", "instapage.com"},
			Response: []string{"You've Discovered A Missing Link. Our Apologies!"},
		},
		"intercom.com": {
			Cname:    []string{"custom.intercom.help"},
			Response: []string{"This page is reserved for artistic dogs.", "<h1 class=\"headline\">Uh oh. That page doesn’t exist.</h1>"},
		},
		"jetbrains.com": {
			Cname:    []string{"myjetbrains.com"},
			Response: []string{"is not a registered InCloud YouTrack."},
		},
		"jfrog.com": {
			Cname:              []string{".jfrog.com"},
			HttpLocationHeader: []string{"www.jfrog.com/error?"},
		},
		"jobs2web.com": {
			Cname: []string{".jobs2web.com"},
		},
		"kajabi.com": {
			Cname:    []string{"endpoint.mykajabi.com"},
			Response: []string{"<h1>The page you were looking for doesn't exist.</h1>"},
		},
		"launchpad6.com": {
			Cname: []string{".launchpad6.com"},
		},
		"mailchimp.com": {
			Cname:              []string{"dkim.mcsv.net"},
			SkipHttpCheck:      true, // domain is only used for dkim entry
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"mailgun.com": {
			Cname: []string{"mailgun.org"},
		},
		"mashery.com": {
			Cname:    []string{"mashery.com"},
			Response: []string{"Unrecognized domain <strong>"},
		},
		"messagelabs.com": {
			Cname: []string{".messagelabs.com"},
		},
		"netlify.com": {
			Cname:    []string{".netlify.com", ".netlify.app"},
			Response: []string{"Not Found"},
		},
		"nitrosell.com": {
			Cname: []string{".nitrosell.com"},
		},
		"office365.com-Discovery": {
			Cname: []string{"autodiscover.outlook.com", "webdir.online.lync.com", "sipdir.online.lync.com",
				"clientconfig.microsoftonline-p.net", "enterpriseregistration.windows.net",
				"enterpriseenrollment-s.manage.microsoft.com", "enterpriseenrollment.manage.microsoft.com",
				"login.microsoftonline.com"},
			SkipHttpCheck:      true, // domains are only used to verify
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"optimy.com": {
			Cname:    []string{".optimytool.com"},
			Response: []string{"Bad URL"},
		},
		"ovh.de": {
			Cname: []string{".ovh.net", ".ovh.de"},
		},
		"pagekite.net": {
			Cname: []string{".pagekite.me"},
		},
		"pantheon.io": {
			Cname:    []string{"pantheonsite.io"},
			Response: []string{"The gods are wise", "The gods are wise, but do not know of the site which you seek."},
		},
		"pingdom.com": {
			Cname:    []string{"stats.pingdom.com"},
			Response: []string{"pingdom"},
		},
		"presstige.at": {
			Cname:    []string{".presstige.at"},
			Response: []string{"Invalid Hostname"},
		},
		"prezly.com": {
			Cname:              []string{".prezly.com"},
			SkipHttpCheck:      true, // domain entries are used for dkim
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification/dkim
		},
		"proposify.com": {
			Cname:    []string{"proposify.biz"},
			Response: []string{"If you need immediate assistance, please contact <a href=\"mailto:support@proposify.biz"},
		},
		"qr-code-generator.com": {
			Cname: []string{"cname.q-r.to"},
		},
		"quad.com": {
			Cname:    []string{".quad.com"},
			Response: []string{"<h2>Not Found</h2>", "HTTP Error 404. The requested resource is not found."},
		},
		"rackspace.com-CDN": {
			Cname: []string{".rackcdn.com"},
		},
		"raynet.de": {
			Cname:    []string{".raynet.de"},
			Response: []string{"403 - Verboten: Zugriff verweigert."},
		},
		"readme.com": {
			Cname:    []string{".readmessl.com", ".readme.io"},
			Response: []string{"Project doesnt exist... yet!"},
		},
		"rebrandly.com": {
			Cname: []string{"rebrandlydomain.com"},
		},
		"rockcontent.com": {
			Cname: []string{".site.scribblelive.com", ".rockstage.io"},
		},
		"scene7.com": {
			Cname:    []string{".scene7.com"},
			Response: []string{"HTTP Status 404 - Not Found"},
		},
		"sectigo.com": {
			Cname:              []string{".sectigo.com"},
			SkipHttpCheck:      true, // only used for domain validation
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"secureserver.net": {
			Cname: []string{".secureserver.net"},
		},
		"sendgrid.net": {
			Cname:              []string{"sendgrid.net"},
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"shopcreator.com": {
			Cname: []string{"shopcreator.net"},
		},
		"shopify.de": {
			Cname:    []string{".myshopify.com", "h.ssl.shopify.com."},
			Response: []string{"Sorry, this shop is currently unavailable.", "Only one step left!"},
		},
		"simplebooklet.com": {
			Cname:    []string{"simplebooklet.com"},
			Response: []string{"We can't find this <a href=\"https://simplebooklet.com"},
		},
		"sitespect.com": {
			Cname:    []string{".ssopt.net"},
			Response: []string{"Invalid URL", "400 Bad Request"},
		},
		"smartling.com": {
			Cname:    []string{"smartling.com"},
			Response: []string{"Domain is not configured"},
		},
		"smartsheet.com": {
			Cname: []string{".smartsheet.com"},
		},
		"smugmug.com": {
			Cname: []string{"domains.smugmug.com"},
		},
		"splashthat.com": {
			Cname:    []string{"domains.splashthat.com", ".splashthat.com"},
			Response: []string{"This event has passed. Thank you!", "unknown domain"},
		},
		"squarespace.com": {
			Cname:    []string{"verify.squarespace.com", "ext-cust.squarespace.com"},
			Response: []string{"SITE DELETED", "This site has been deleted by the owner."},
		},
		"starcloud.us": {
			Cname: []string{".starcloud.us"},
		},
		"statuspage.io": {
			Cname:    []string{"statuspage.io"},
			Response: []string{"Better Status Communication", "You are being <a href=\"https://www.statuspage.io\">redirected"},
		},
		"statwolf.com": {
			Cname:    []string{".statwolf.com"},
			Response: []string{"404 Web Site not found."},
		},
		"surge.sh": {
			Cname:    []string{"surge.sh"},
			Response: []string{"project not found"},
		},
		"surveygizmo.com": {
			Cname:    []string{"privatedomain.sgizmo.com", "privatedomain.surveygizmo.eu", "privatedomain.sgizmoca.com"},
			Response: []string{"data-html-name"},
		},
		"tave.com": {
			Cname:    []string{"clientaccess.tave.com"},
			Response: []string{"<h1>Error 404: Page Not Found</h1>"},
		},
		"teamwork.com": {
			Cname:    []string{"teamwork.com"},
			Response: []string{"Oops - We didn't find your site."},
		},
		"thinkific.com": {
			Cname:    []string{"thinkific.com"},
			Response: []string{"You may have mistyped the address or the page may have moved."},
		},
		"tockify.com": {
			Cname:    []string{".tockify.com"},
			Response: []string{"<title>Tockify | Modern Attractive Website Calendars</title>"},
		},
		"tickaroo.com": {
			Cname:    []string{".tickaroo.com"},
			Response: []string{"404 error"},
		},
		"tictail.com": {
			Cname:    []string{"tictail.com", "domains.tictail.com"},
			Response: []string{"Building a brand of your own?", "to target URL: <a href=\"https://tictail.com", "Start selling on Tictail."},
		},
		"tilda.cc": {
			Cname:    []string{"tilda.ws"},
			Response: []string{"Domain has been assigned"},
		},
		"triancdn.net": {
			Cname:    []string{".triancdn.net"},
			Response: []string{"Welcome to TrianCDN."},
		},
		"troi.at": {
			Cname:    []string{".troi.at"},
			Response: []string{"404 page not found"},
		},
		"tumblr.com": {
			Cname:    []string{".tumblr.com"},
			Response: []string{"There's nothing here.", "Whatever you were looking for doesn't currently exist at this address."},
		},
		"unbounce.com": {
			Cname:    []string{"unbouncepages.com"},
			Response: []string{"The requested URL / was not found on this server.", "The requested URL was not found on this server"},
		},
		"updown.io": {
			Cname:    []string{".status.updown.io", "tls1.updn.io"},
			Response: []string{"<h1>Custom domain status page</h1>"},
		},
		"uservoice.com": {
			Cname:    []string{"uservoice.com"},
			Response: []string{"This UserVoice subdomain is currently available!"},
		},
		"urlgeni.us": {
			Cname: []string{"brand.urlgeni.us"},
		},
		"vendhq.com": {
			Cname:    []string{"vendecommerce.com"},
			Response: []string{"Looks like you've traveled too far into cyberspace."},
		},
		"webflow.com": {
			Cname:    []string{"proxy.webflow.com", "proxy.webflow.io", "proxy-ssl.webflow.com"},
			Response: []string{"<p class=\"description\">The page you are looking for doesn't exist or has been moved.</p>"},
		},
		"wishpond.com": {
			Cname:    []string{"wishpond.com"},
			Response: []string{"https://www.wishpond.com/404?campaign=true"},
		},
		"wix.com": {
			Cname:    []string{".wixdns.net"},
			Response: []string{"Error ConnectYourDomain occurred"},
		},
		"wordpress.com": {
			Cname:    []string{"wordpress.com"},
			Response: []string{"Do you want to register"},
		},
		"wpengine.com": {
			Cname:    []string{".wpengine.com."},
			Response: []string{"The site you were looking for couldn't be found."},
		},
		"yospace.com": {
			Cname: []string{".yospace.com"},
		},
		"yumpu.com": {
			Cname: []string{".yumpu.com"},
		},
		"zendesk.com": {
			Cname:              []string{".zendesk.com"},
			Response:           []string{"<title>Help Center Closed | Zendesk</title>", "Help Center Closed"},
			SkipHttpCheck:      true,
			CnameForVerifyOnly: true, // do not report as possible takeover because it's only used for verification
		},
		"zeroheight.com": {
			Cname: []string{"zeroheight.com"},
		},
	}
	ProviderList []string // alphabetic list of all provider names
	HttpClient   = &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
		Timeout: tools.ClientTimeout,
	}
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "subdomaintakeover",
		Desc: "checks if a subdomain is pointing to a public provider where an attacker can takeover it, takes \"brief\" or \"detailed\" as parameter",
		Init: SubdomainTakeoverInit,
		Flag: config.StringFlag,
	})
}

func SubdomainTakeoverInit(c config.Config) (config.ScannerFunc, config.ScannerFields, error) {
	DetailedResults = c.String("subdomaintakeover") == "detailed"
	DnsResolver = net.JoinHostPort(c.String("resolver"), "53")
	DnsClient.Dialer = &net.Dialer{Timeout: DnsRequestTimeout}
	for p := range Providers {
		ProviderList = append(ProviderList, p)
	}
	sort.Strings(ProviderList) // testing of all providers is done in alphabetic order
	if DetailedResults {
		return SubdomainTakeoverScanner, SubdomainTakeoverDetailedFields, nil
	}
	return SubdomainTakeoverScanner, SubdomainTakeoverFields, nil
}

func SubdomainTakeoverScanner(host config.Host) []string {
	var (
		detectedProviderName     = "UNKNOWN"
		detectionMethod          = ""
		detectedTakeoverPossible = "NO"
	)

	targetCNAMEs, _ := doCNAMELookup(host.Testing) // get all CNAMEs of Domain

	if len(targetCNAMEs) > 0 {
		targetCname := targetCNAMEs[len(targetCNAMEs)-1] // detection of Provider is based on the last CNAME in chain

		aRecords, _ := doALookup(host.Testing)
		aaaaRecords, _ := doAAAALookup(host.Testing)
		hasIPRecords := len(aRecords) > 0 || len(aaaaRecords) > 0

		if !hasIPRecords {
			// cname points to a dns record without any A or AAAA record - maybe the destination record does not exist anymore
			detectionMethod = "NO-IP-RECORDS"
			detectedTakeoverPossible = "MAYBE"
		}

		for _, providerName := range ProviderList {
			provider := Providers[providerName]

			// try to detect provider using Regex Pattern of source dns entry
			if provider.SourceDnsEntryRegex != "" {
				regexMatched, err := regexp.MatchString(provider.SourceDnsEntryRegex, host.Testing)
				if err != nil {
					log.WithError(err).Errorf("Problem in Regex-Pattern - %s", provider.SourceDnsEntryRegex)
					// There is a problem with the regex pattern
					if DetailedResults {
						return []string{SubdomainTakeoverErrorRegex, detectedProviderName, detectionMethod, detectedTakeoverPossible}
					}
					return []string{"E", "E"}
				}
				if regexMatched {
					detectedProviderName = providerName
					detectionMethod += "+CNAME-SOURCE-REGEX"
				}
			}

			// try to detect provider using Regex Pattern Match of CNAME
			if provider.CnameRegex != "" {
				regexMatched, err := regexp.MatchString(provider.CnameRegex, targetCname)
				if err != nil {
					log.WithError(err).Errorf("Problem in Regex-Pattern - %s", provider.CnameRegex)
					// There is a problem with the regex pattern
					if DetailedResults {
						return []string{SubdomainTakeoverErrorRegex, detectedProviderName, detectionMethod, detectedTakeoverPossible}
					}
					return []string{"E", "E"}
				}
				if regexMatched {
					detectedProviderName = providerName
					detectionMethod += "+CNAME-REGEX"
				}
			}

			// try to detect provider using list of possible CNAMEs
			if provider.Cname != nil {
				for _, cname := range provider.Cname {
					if strings.Contains(targetCname, cname) {
						detectedProviderName = providerName
						detectionMethod += "+CNAME"
						break
					}
				}
			}

			var providerViaCnameDetected = strings.Contains(detectionMethod, "CNAME")

			// rank CNAMEs that are only used for verification as "false" and not as "maybe"
			// this will remove CNAME entries like domain verification, certificate validation, ...
			if providerViaCnameDetected && !hasIPRecords && provider.CnameForVerifyOnly == true {
				detectionMethod += "+VERIFY-ONLY"
				detectedTakeoverPossible = "NO"
			}

			// if CNAME detection was successful and IP Records exists, we can improve it with HTTP request information
			if providerViaCnameDetected && hasIPRecords && provider.SkipHttpCheck == false {
				resp, err := HttpClient.Get("https://" + host.Testing)
				if err != nil {
					log.WithError(err).Errorf("HTTP Request error - %s", host.Testing)
					// Error while requesting URL content
					if DetailedResults {
						return []string{SubdomainTakeoverErrorHTTP, detectedProviderName, detectionMethod, detectedTakeoverPossible}
					}
					return []string{"E", "E"}
				}

				if provider.HttpLocationHeader != nil {
					// do takeover detection based on location http header
					for _, location := range provider.HttpLocationHeader {
						if strings.Contains(resp.Header.Get("Location"), location) {
							detectionMethod += "+HTTP-LOCATION"
							detectedTakeoverPossible = "YES"
							break
						}
					}
				} else if provider.HttpStatusCode != 0 && provider.HttpStatusCode == resp.StatusCode {
					if provider.HttpServerHeaderMustMatch == "" {
						// do takeover detection based on status code
						detectionMethod += "+HTTP-STATUS-CODE"
						detectedTakeoverPossible = "YES"
					} else if resp.Header.Get("server") == provider.HttpServerHeaderMustMatch {
						// do takeover detection based on status code + validate if http server header is correct
						detectionMethod += "+HTTP-STATUS-CODE+HTTP-SERVER"
						detectedTakeoverPossible = "YES"
					}
				} else {
					// do takeover detection based on content parts
					bodyContent, err := ioutil.ReadAll(resp.Body)
					if err != nil {
						log.WithError(err).Errorf("Error while reading Response body of %s", host.Testing)
						if DetailedResults {
							return []string{SubdomainTakeoverErrorReadBody, detectedProviderName, detectionMethod, detectedTakeoverPossible}
						}
						return []string{"E", "E"}
					}

					if provider.Response != nil {
						for _, response := range provider.Response {
							if strings.Contains(string(bodyContent), response) {
								detectionMethod += "+HTTP-RESPONSE"
								detectedTakeoverPossible = "YES"
								break
							}
						}
					}
				}
				resp.Body.Close()
			}

			if providerViaCnameDetected {
				break // end detection execution if we already have provider match via CNAME detection
			}
		}
	}
	if DetailedResults {
		return []string{"", detectedProviderName, detectionMethod, detectedTakeoverPossible}
	}
	return []string{detectedProviderName, detectedTakeoverPossible}
}

func doCNAMELookup(host string) ([]string, error) {
	return doGenericLookup(host, dns.TypeCNAME,
		func(rr dns.RR) string {
			if v, ok := rr.(*dns.CNAME); ok {
				return strings.ToLower(v.Target)
			}
			return ""
		})
}

func doALookup(host string) ([]string, error) {
	return doGenericLookup(host, dns.TypeA,
		func(rr dns.RR) string {
			if v, ok := rr.(*dns.A); ok {
				return v.A.String()
			}
			return ""
		})
}

func doAAAALookup(host string) ([]string, error) {
	return doGenericLookup(host, dns.TypeAAAA,
		func(rr dns.RR) string {
			if v, ok := rr.(*dns.AAAA); ok {
				return v.AAAA.String()
			}
			return ""
		})
}

func doGenericLookup(host string, recordType uint16, converterFunc func(dns.RR) string) ([]string, error) {
	m := new(dns.Msg)
	m.SetQuestion(dns.Fqdn(host), recordType)
	m.RecursionDesired = true

	r, _, err := DnsClient.Exchange(m, DnsResolver)
	if err != nil || r == nil || r.Rcode != dns.RcodeSuccess {
		if err != nil {
			log.WithError(err).Errorf("Error while resolving - %s", host)
		}
		return []string{}, err // error while resolving
	}
	var result []string
	for _, record := range r.Answer {
		if val := converterFunc(record); val != "" {
			result = append(result, val)
		}
	}
	return result, nil
}
