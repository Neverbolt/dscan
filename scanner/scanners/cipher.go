package scanners

import (
	"crypto/tls"
	"gitlab.com/Neverbolt/dscan/scanner"
	"net/url"

	"gitlab.com/Neverbolt/dscan/config"

	log "github.com/Sirupsen/logrus"
)

var (
	ciphers = []uint16{
		tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
		tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
		tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
		tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
		tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
		tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,
		tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
		tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256,
		tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
		tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
		tls.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,
	}

	// CipherFields are all fields generated by the SSLScan method
	CipherFields = []string{"connection_cipher_suite"}
	// CipherError is the error returned by SSLScan
	CipherError = []string{"E"}
	// CipherNone is returned when the target is not reachable via https
	CipherNone = []string{""}
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name:    "ciphercheck",
		Desc:    "checks if the used cipher is in a list of secure ciphers, a very very much simplified version of ssl rating",
		Scanner: CipherScan,
		Fields:  CipherFields,
		Flag:    config.BoolFlag,
	})
}

// CipherScan checks the ssl score of the domain
func CipherScan(host config.Host) (result []string) {
	if !host.Online {
		return CipherNone
	}

	u, err := url.Parse(host.URL)
	if err != nil {
		log.Debugf("SSLSCAN Could not parse host URL '%s' for SSLScan: %s", host.URL, err)
		return SSLError
	}

	if u.Scheme != "https" {
		return SSLNone
	}

	cfg := tls.Config{}
	conn, err := tls.Dial("tcp", u.Host+":443", &cfg)
	if err != nil {
		log.Debugf("Could not dial https host '%s:443' for SSLScan: %s", u.Host, err)
		return SSLError
	}
	defer conn.Close()

	return []string{
		CheckCipherSuite(conn),
	}
}

// CheckCipherSuite checks if the used cipher is in the list of allowed ciphers
func CheckCipherSuite(conn *tls.Conn) string {
	suite := conn.ConnectionState().CipherSuite

	for _, sec := range ciphers {
		if suite == sec {
			return "secure"
		}
	}

	return "insecure"
}
