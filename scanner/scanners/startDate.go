package scanners

import (
	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/scanner"
	"time"
)

var (
	startDateFields = []string{"startdate"}
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "startdate",
		Desc: "adds a column containing the starting date in a given format (use 'simple' for DD.MM.YYYY, otherwise see https://golang.org/pkg/time/#Time.Format)",
		Init: StartDateInit,
		Flag: config.StringFlag,
	})
}

// StartDateInit loads the date and puts it into the given format
func StartDateInit(c config.Config) (config.ScannerFunc, config.ScannerFields, error) {
	format := c.String("startdate")
	if format == "simple" {
		format = "02.01.2006"
	}

	result := make([]string, 1)
	result[0] = time.Now().Format(format)

	wrapperFunc := func(h config.Host) []string {
		return result
	}

	return wrapperFunc, startDateFields, nil
}

