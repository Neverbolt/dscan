package scanners

import (
	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/scanner"
	"gitlab.com/Neverbolt/dscan/tools"
	"golang.org/x/net/html"
	"io"
	"strings"
)

var (
	// CSPFields are all fields returned by CSPCheckScan
	CSPFields = []string{"Content-Security-Policy-Mode", "Content-Security-Policy"}
	// CSPError is the error that CSPScanner can return
	CSPError = []string{"E","E"}
	// CSPNone is returned when the host is not online
	CSPNone = []string{"",""}

	cspModeBlock      = "Block"
	cspModeReportOnly = "Report-Only"
)

func init() {
	scanner.RegisterScanner(config.Scanner{
		Name: "csp",
		Desc: "checks if a content security policy can be found in the HTTP headers or as meta tag and returns it with the applicable mode",
		Scanner: CSPScanner,
		Fields: CSPFields,
		Flag: config.BoolFlag,
	})
}

// HeaderScanner loads all Header fields that have been requested by the CLI
func CSPScanner(host config.Host) []string {
	if !host.Online {
		return CSPNone
	}

	resp, err := tools.Request.Get(host.URL)
	if err != nil {
		return CSPError
	}

	policy := resp.Header.Get("Content-Security-Policy")
	if policy != "" {
		return []string{cspModeBlock, policy}
	}

	policy = resp.Header.Get("Content-Security-Policy-Report-Only")
	if policy != "" {
		return []string{cspModeReportOnly, policy}
	}

	return extractCSPMeta(resp.Body)
}

func extractCSPMeta(body io.ReadCloser) []string {
	z := html.NewTokenizer(body)

	for {
		tt := z.Next()

		switch {
		case tt == html.StartTagToken:
			if success, mode, res := handleToken(z.Token()); success {
				return []string{mode, res}
			}
		case tt == html.SelfClosingTagToken:
			if success, mode, res := handleToken(z.Token()); success {
				return []string{mode, res}
			}
		case tt == html.ErrorToken:
			// End of the document, we're done
			return []string{"", ""}
		}
	}
}

func handleToken(t html.Token) (bool, string, string) {
	if !strings.EqualFold(t.Data, "meta") {
		return false, "", ""
	}

	found := false
	cspType := ""
	for _, attr := range t.Attr {
		if strings.EqualFold(attr.Key, "http-equiv") {
			if strings.EqualFold(attr.Val, "content-security-policy") {
				found = true
				cspType = cspModeBlock
				break
			}

			if strings.EqualFold(attr.Val, "content-security-policy-report-only") {
				found = true
				cspType = cspModeReportOnly
				break
			}
		}
	}

	if found {
		for _, attr := range t.Attr {
			if strings.EqualFold(attr.Key, "content") {
				return true, cspType, attr.Val
			}
		}
	}

	return false, "", ""
}

