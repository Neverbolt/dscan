package scanner

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.com/Neverbolt/dscan/tools"
	"net/http"
	"net/url"
)

// GetFinalURL loads the final url after redirects for the given starting point
func GetFinalURL(url string) string {
	log.Debugf("Loading final URL for %s", url)
	https := true
	nextURL := "https://" + url

	var resp *http.Response
	var err error
	for i := 0; i < 100; i++ {
		client := &http.Client{
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
			Transport: &http.Transport{
				//TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
			Timeout: tools.ClientTimeout,
		}

		resp, err = client.Get(nextURL)
		if err != nil {
			if https {
				https = false
				nextURL = "http://" + url
				continue
			}

			resp, err = client.Get(nextURL)
			if err != nil {
				log.Debugf("Redirect Error for %s: %s", url, err)
				return ""
			}
		}
		defer resp.Body.Close()

		if resp.StatusCode == 200 {
			break
		} else if 299 < resp.StatusCode && resp.StatusCode < 399 {
			nextURL = getNextLocation(nextURL, resp.Header.Get("Location"))
		} else {
			break
		}
	}

	log.Debugf("Found final url '%s' for '%s'", nextURL, url)
	return nextURL
}

func getNextLocation(current, next string) string {
	// when updating this also update the same function in scanner/scanners/redirect.go
	currentURL, err := url.Parse(current)
	if err != nil {
		return ""
	}
	nextURL, err := url.Parse(next)
	if err != nil {
		return ""
	}

	return currentURL.ResolveReference(nextURL).String()
}

