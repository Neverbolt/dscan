package scanner

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.com/Neverbolt/dscan/config"
)

var (
	// Scanners is the list of all available scanners
	Scanners = make([]config.Scanner, 0)
)

// RegisterScanner registers a new scanner, this function should be called in the init() function of the scanner file
func RegisterScanner(scanner config.Scanner) {
	log.Debugf("Registering scanner %s", scanner.Name)
	Scanners = append(Scanners, scanner)
}

// DomainPrinter just prints the domain name for entry identification
func DomainPrinter(host config.Host) []string {
	return []string{host.Testing}
}

// SuccessPrinter is needed so initial domain resolution has a field to flag fails in
func SuccessPrinter(host config.Host) []string {
	if host.Online {
		return []string{"success"}
	} else {
		return []string{"error"}
	}
}

// useScanner signals if the scanner is to be used
func useScanner(all bool, s config.Scanner, c config.Config) bool {
	if s.Flag == config.BoolFlag {
		return all || c.Bool(s.Name)
	} else if s.Flag == config.StringFlag {
		return c.String(s.Name) != ""
	} else if s.Flag == config.StringSliceFlag {
		return len(c.StringSlice(s.Name)) > 0
	}

	log.Fatalf("Invalid flag type %d\n", s.Flag)
	return false
}

// RegisterScanners returns all enabled scanners
func RegisterScanners(c config.Config) (config.ScannerFields, []config.Scanner) {
	// The two standard scanners are always needed to signal which domain is being processed and if the initial resolve has succeeded
	fields := []string{"domain", "success"}
	scanners := []config.Scanner{
		{Name: "", Desc: "prints the domain", Scanner: DomainPrinter, Fields: []string{"domain"}},
		{Name: "", Desc: "prints 'success'", Scanner: SuccessPrinter, Fields: []string{"success"}},
	}

	// Check if all boolean flags should just be set to true
	all := c.Bool("all")
	if all {
		log.Infof("Using all scans")
	}

	// Register each scanner if its
	for _, scanner := range Scanners {
		if useScanner(all, scanner, c) {
			// Initialize it if there is an init method
			if scanner.Init != nil {
				scannerFunc, scannerFields, err := scanner.Init(c)
				if err != nil {
					log.Fatalf("Could not initialize Scanner '%s': %s\n", scanner.Name, err)
				}

				scanner = config.Scanner{Name: scanner.Name, Desc: scanner.Desc, Scanner: scannerFunc, Fields: scannerFields, Init: scanner.Init, Flag: scanner.Flag}
			}

			// Append all scanner fields and the scanner itself
			fields = append(fields, scanner.Fields...)
			scanners = append(scanners, scanner)
		}
	}

	return fields, scanners
}
