package scanner

import (
	"sync"

	"gitlab.com/Neverbolt/dscan/config"

	log "github.com/Sirupsen/logrus"
)

// Job gets done by worker
type Job struct {
	JobID  int
	Work   config.ScannerFunc
	Host   config.Host
	Output []string
	WG     *sync.WaitGroup
}

// SpawnWorkers creates a given number of workers that listen on the queue
func SpawnWorkers(count uint, queue chan Job) *sync.WaitGroup {
	// wg is the WaitGroup keeping track of all running workers
	wg := &sync.WaitGroup{}

	for i := uint(0); i < count; i++ {
		wg.Add(1)
		go worker(wg, queue, i)
	}

	log.Infof("Spawned %d workers\n", count)

	return wg
}

// worker gets the job done
// it takes the Work, applies it to Domain, sets the Output and then calls done on WG
func worker(wg *sync.WaitGroup, queue chan Job, i uint) {
	log.Infof("Started worker Goroutine %d\n", i)

	// Take on all jobs that can be gotten
	for job := range queue {
		log.Debugf("%2d Got new job: %d", i, job.JobID)
		// Execute them
		result := job.Work(job.Host)
		log.Debugf("%2d finished job %d", i, job.JobID)
		// Write the result to the output holder
		for i, res := range result {
			job.Output[i] = res
		}
		log.Debugf("%2d placed results for %d", i, job.JobID)
		// Mark job as done (and get paid)
		job.WG.Done()
		log.Debugf("%2d marked %d done", i, job.JobID)
	}

	// Signal worker being ready to call it a day
	wg.Done()
}
