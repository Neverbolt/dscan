package tools

import (
	"fmt"
	"runtime"
	"sync"
	"time"

	"github.com/cloudfoundry/gosigar"
	log "github.com/Sirupsen/logrus"
)

const (
	// maxMemoryPercent is how many percent of memory we allow us to take before we start evicting aggressively
	maxMemoryPercent = 50
	// cacheCleanup after how many times of storing a cleanup needs to be triggered
	cacheCleanup = 50
	// cacheMaxAge is how old the cacheEntries may be before being freed by a cache Cleanup
	cacheMaxAge = 5 * time.Minute
	// cacheMaxAgeSteps is the steps in which the cacheMaxAge is decreased until memory pressure is low enough
	cacheMaxAgeSteps = 1 * time.Minute
)

var (
	// totalAllowedMemory is the total amount of memory that we allow ourselves to take
	// we assume that by default there are at least 512MB of memory for us, but this number is only used if getting the
	// actual amount of RAM in the system fails
	totalAllowedMemory = uint64(512*1024*1024)
)

func init() {
	mem := sigar.Mem{}
	err := mem.Get()
	if err != nil {
		log.WithError(err).Error("Could not determine actual amount of memory, falling back to 512MB")
		return
	}

	totalAllowedMemory = mem.Total/100*maxMemoryPercent
}

// The Cache is a thread safe construct that can cache values and supports an "In Progress" state
type Cache struct {
	// InProgressValue indicates that a given key is already being worked on
	InProgressValue interface{}
	// Tries is the number of times a waiting access tries to load an in progress value
	Tries int
	// Timeout is the time spent between each waiting access
	Timeout time.Duration

	// the cache should be filled with cacheEntry structs to allow garbage collection
	// all non-cacheEntry values will not be garbage collected and can cause out-of-memory situations
	cache *sync.Map
	// does not need to be synchronized, since actual value is not that important
	sinceLastCleanup int
}

type cacheEntry struct {
	T time.Time
	V interface{}
}

// NewCache returns a cache that is initialized to default values
// It is advised to build a custom cache with BuildCache
func NewCache() *Cache {
	return BuildCache(-1, 10, 100*time.Millisecond)
}

// BuildCache creates a cache according to the parameters
// The inProgressValue should be of another type than the values stored in the cache to avoid confusion between the two
func BuildCache(inProgressValue interface{}, tries int, timeout time.Duration) *Cache {
	return &Cache{
		InProgressValue: inProgressValue,
		Tries:           10,
		Timeout:         timeout,
		cache:           &sync.Map{},
	}
}

// CheckCache checks if a value is in the cache
func (c *Cache) CheckCache(key interface{}) (val interface{}, inProgress bool, ok bool) {
	val, ok = c.cache.Load(key)
	if !ok {
		return nil, false, false
	}

	if val == c.InProgressValue {
		return val, true, true
	}

	return val, false, true
}

// LoadOrReserve either loads a present value from the cache, or reserves one to be inserted later on
// if loaded is false then a reservation was done
func (c *Cache) LoadOrReserve(key interface{}) (val interface{}, loaded bool) {
	v, loaded := c.cache.LoadOrStore(key, c.InProgressValue)

	if v == c.InProgressValue {
		if loaded {
			return c.Load(key)
		}

		return c.InProgressValue, loaded
	}

	if e, ok := v.(cacheEntry); ok {
		return e.V, true
	}
	return v, loaded
}

// Load loads a value from the cache and waits for Tries * Timeout seconds if it is "in progress"
func (c *Cache) Load(key interface{}) (val interface{}, success bool) {
	for i := 0; i < c.Tries; i++ {
		if i != 0 {
			log.Debugf("Cache wait #%d for %v\n", i, key)
			time.Sleep(c.Timeout)
		}

		if v, ok := c.cache.Load(key); ok {
			if v != c.InProgressValue {
				if e, ok := v.(cacheEntry); ok {
					return e.V, true
				}
				return v, true
			}
		} else {
			break
		}
	}

	return nil, false
}

// Store stores an entry in the cache
// it also calls Cleanup every cacheCleanup calls
func (c *Cache) Store(key, val interface{}) {
	c.sinceLastCleanup++
	if c.sinceLastCleanup%cacheCleanup == 0 {
		go c.Cleanup()
	}

	if len(fmt.Sprintf("%+v", val)) < 100 {
		log.Debugf("Storing for %+v: %+v", key, val)
	} else {
		log.Debugf("Storing for %+v", key)
	}

	c.cache.Store(key, cacheEntry{T: time.Now(), V: val})
}

// Delete simply deletes an entry from the cache
func (c *Cache) Delete(key interface{}) {
	log.Debugf("Deleting %+v", key)
	c.cache.Delete(key)
}

// Cleanup checks the memory pressure and triggers garbage collection / cleanage of old cache entries
func (c *Cache) Cleanup() {
	log.Debugf("Running cleanup for cache")
	cleaned := 0

	m := runtime.MemStats{}
	currentMaxAge := cacheMaxAge

	for {
		now := time.Now()
		c.cache.Range(func(k interface{}, v interface{}) bool {
			if e, ok := v.(cacheEntry); ok {
				if e.T.Add(currentMaxAge).Before(now) {
					c.Delete(k)
					cleaned++
				}
			}
			return true
		})
		runtime.GC()

		runtime.ReadMemStats(&m)
		if m.Alloc < totalAllowedMemory {
			break
		}
		if currentMaxAge==0 {
			log.Errorf("Still above allowed memory after cleaning the entire cache, things may get dicey now")
			log.Errorf("Alloc = %v MiB", bToMb(m.Alloc))
			log.Errorf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
			log.Errorf("\tSys = %v MiB", bToMb(m.Sys))
			log.Errorf("\tNumGC = %v\n", m.NumGC)
			break
		}
		if currentMaxAge<cacheMaxAgeSteps {
			currentMaxAge = 0
		} else {
			currentMaxAge = currentMaxAge - cacheMaxAgeSteps
		}
	}

	log.Debugf("Cleaned %d entries", cleaned)
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
