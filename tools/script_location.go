package tools

import (
	"fmt"
	"os"
	"path"
)

// GetScriptLocation resolves where the given script lies
// this is done in relation to the config parameters, current and executable directory
func GetScriptLocation(name, configdir string) (string, error) {
	// if the script location itself is absolute then only check this
	if path.IsAbs(name) {
		return fileExists(name)
	}

	// if an absolute config dir was given look relative to it
	if len(configdir) > 0 {
		// try absolute or relative to current dir
		loc, err := fileExists(configdir, name)
		// if config dir is absolute then fail
		if path.IsAbs(configdir) && err != nil {
			return loc, err
		}

		// found it
		if err == nil {
			return loc, err
		}
	}

	// try path relative to executable
	ex, err := os.Executable()
	if err != nil {
		return "", fmt.Errorf("Could not get executable location: %s", err)
	}
	return fileExists(path.Dir(ex), configdir, name)
}

func fileExists(locs ...string) (string, error) {
	loc := path.Join(locs...)
	if _, err := os.Stat(loc); err != nil {
		return loc, fmt.Errorf("Could not find local file at '%s': %s", loc, err)
	}

	return loc, nil
}
