package tools

import (
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"time"

	"gitlab.com/Neverbolt/dscan/config"

	log "github.com/Sirupsen/logrus"
)

const (
	// ClientTimeout is how long an HTTPHandler client has until it times out
	ClientTimeout = time.Duration(5 * time.Second)
)

var (
	// Request is to be used for all HTTP requests
	Request = HTTPHandler{
		cache: BuildCache(-1, 20, 100*time.Microsecond),
		client: http.Client{
			Timeout: ClientTimeout,
		},
	}
)

// InitHTTP loads all parameters into the Request object
func InitHTTP(c config.Config) {
	Request.UserAgent = c.String("useragent")
}

type requestCacheEntry struct {
	request *http.Request
	body    []byte
}

// HTTPHandler is the caching handler for all HTTP connections
// It supports spwaning custom connections with cookies and other values pre-set
type HTTPHandler struct {
	cache *Cache

	// HTTP is the client to be used by all commands
	client http.Client

	// UserAgent is the user agent presented by the Handler
	UserAgent string
}

// Get sends a GET request without additional cookies or headers to the given URL
func (h *HTTPHandler) Get(url string) (*http.Response, error) {
	return h.GGet(url, nil, nil)
}

// GGet sends a GET request with the given cookies and headers to the given URL
func (h *HTTPHandler) GGet(url string, cookies *map[string]string, headers *map[string]string) (*http.Response, error) {
	return h.Request("GET", url, cookies, headers)
}

// Request starts an arbitrary request
func (h *HTTPHandler) Request(method, url string, cookies *map[string]string, headers *map[string]string) (*http.Response, error) {
	key := buildCacheKey(method, url, cookies, headers)

	if c, loaded := h.cache.LoadOrReserve(key); loaded {
		entry := c.(requestCacheEntry)
		resp, err := http.ReadResponse(bufio.NewReader(bytes.NewReader(entry.body)), entry.request)
		if err != nil {
			log.Errorf("could not re-read stored response of request for %s: %s", err)
			return nil, err
		}
		return resp, nil
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		h.cache.Delete(key)
		log.Errorf("could not create request for %s: %s", url, err)
		return nil, err
	}

	if cookies != nil {
		for k, v := range *cookies {
			req.AddCookie(&http.Cookie{Name: k, Value: v})
		}
	}

	if h.UserAgent != "" {
		req.Header.Set("User-Agent", h.UserAgent)
	}

	if headers != nil {
		for k, v := range *headers {
			req.Header.Add(k, v)
		}
	}

	resp, err := h.client.Do(req)
	if err != nil {
		h.cache.Delete(key)
		log.Errorf("could not execute request for %s: %s", url, err)
		return nil, err
	}

	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		h.cache.Delete(key)
		log.Errorf("could not dump response for request for %s: %s", url, err)
		return nil, err
	}

	io.Copy(ioutil.Discard, resp.Body) // Done to allow for keepalive, since go is a bit trash
	defer resp.Body.Close()

	entry := requestCacheEntry{req, dump}
	h.cache.Store(key, entry)
	h.cache.Store(buildCacheKey(method, resp.Request.URL.String(), cookies, headers), entry)

	resp, err = http.ReadResponse(bufio.NewReader(bytes.NewReader(dump)), req)
	if err != nil {
		log.Errorf("could not re-read stored response of request for %s: %s", err)
		return nil, err
	}
	return resp, nil
}

func buildCacheKey(method, url string, cookies *map[string]string, headers *map[string]string) string {
	key := method + url + ";"

	if cookies != nil {
		for k, v := range *cookies {
			key += "c" + k + ":" + v + ";"
		}
	}

	if headers != nil {
		for k, v := range *headers {
			key += "h" + k + ":" + v + ";"
		}
	}

	return key
}
