package main

import (
	"fmt"
	"gitlab.com/Neverbolt/dscan/config"
	"gitlab.com/Neverbolt/dscan/scanner"
	"os"
	"testing"

	log "github.com/Sirupsen/logrus"
)

var (
	BaseConfig = TestConfig{
		Domains: []string{"google.com"},
		Config:  map[string]interface{}{
			"connections": 1,
			//"csp-check": true,
		},
	}
)

func Benchmark(b *testing.B) {
	log.SetOutput(os.Stderr)
	log.SetLevel(log.DebugLevel)

	c := BaseConfig

	// Create the input and output connections
	// inlet is from the domain file streams to the scanners
	inlet := make(chan string, c.Int("connections")*2)
	// outlet is from the scanners to the output stream
	outlet := make(chan *[]string, c.Int("connections")*2)
	// writerDone is closed as soon as the last entry has been written by WriteOut
	writerDone := make(chan bool, 1)

	// demo input
	// go config.ReadFiles([]string{"at_domain_shortest.txt"}, inlet)
	go func(inlet chan<- string) {
		inlet<-"google.com"
		close(inlet)
	}(inlet)

	// demo output
	go func(outlet <-chan *[]string) {
		i := 0
		for out := range outlet {
			fmt.Printf("%d - %s\n", i, (*out)[0])
			i++
		}
	}(outlet)

	err := scanner.Scan(&c, inlet, outlet)
	if err != nil {
		panic(err)
	}

	// close outlet and wait for it to be emptied
	close(outlet)
	<-writerDone
}

type TestConfig struct {
	Format  string
	Domains []string
	Config  map[string]interface{}
	config  config.Config
}

func (r *TestConfig) Int(name string) int {
	v, ok := r.Config[name]
	if !ok {
		if r.config != nil {
			return r.config.Int(name)
		}
		return 0
	}

	if val, ok := v.(int); ok {
		return val
	}

	return 0
}

func (r *TestConfig) Bool(name string) bool {
	v, ok := r.Config[name]
	if !ok {
		if r.config != nil {
			return r.config.Bool(name)
		}
		return false
	}

	if val, ok := v.(bool); ok {
		return val
	}

	return false
}

func (r *TestConfig) String(name string) string {
	v, ok := r.Config[name]
	if !ok {
		if r.config != nil {
			return r.config.String(name)
		}
		return ""
	}

	if val, ok := v.(string); ok {
		return val
	}

	return ""
}

func (r *TestConfig) StringSlice(name string) []string {
	v, ok := r.Config[name]
	if !ok {
		if r.config != nil {
			return r.config.StringSlice(name)
		}
		return []string{}
	}

	if val, ok := v.([]string); ok {
		return val
	}

	return []string{}
}
