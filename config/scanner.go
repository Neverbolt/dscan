package config

// ScannerFunc is a function that takes a domain and scans it for a certain action
type ScannerFunc func(host Host) (result []string)

// ScannerFields are all fields returned by a scanner
type ScannerFields []string

// InitFunc initializes a scanner and returns the initialized function as well as its output
type InitFunc func(c Config) (ScannerFunc, ScannerFields, error)

// FlagType is the type of cli flag that the scanner uses
type FlagType int

const (
	// BoolFlag is a simple toggle flag
	BoolFlag = iota
	// StringFlag takes a string parameter
	StringFlag = iota
	// StringSliceFlag takes a list of string parameters
	StringSliceFlag = iota
)

// Scanner is a struct holding all information about a scanner
type Scanner struct {
	Name    string
	Desc    string
	Scanner ScannerFunc
	Fields  ScannerFields
	Init    InitFunc
	Flag    FlagType
}
