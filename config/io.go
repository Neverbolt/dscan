package config

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	log "github.com/Sirupsen/logrus"
)

const (
	transformToEmpty = -1
)

var (
	newlineEscape = strings.NewReplacer("\n", "\\n")
)

// Outlet is a channel through which results are returned to the user
type Outlet chan<- *[]string

// Inlet is a channel through which the requested domains come from the user
type Inlet <-chan string

// ReadFiles reads all given files and writes them line by line into the inlet channel
func ReadFiles(files []string, inlet chan string) {
	log.Info("Started to read files")

	// Read file by file, line by line to allow very large files (probably overkill)
	for _, fileName := range files {
		readFile(fileName, inlet)
	}

	log.Infof("Finished reading %d files", len(files))
	// Close the inlet connection to let other parts know that it's over
	close(inlet)
}

func readFile(fileName string, inlet chan string) {
	log.Infof("Reading from file '%s'", fileName)

	file, err := os.Open(fileName)
	if err != nil {
		log.Errorf("Could not open file '%s': %s", fileName, err)
		return
	}
	defer file.Close()

	var line string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		log.Debugf("Reading new line")
		line = scanner.Text()
		if len(line) != 0 {
			inlet <- line
		}
		log.Debugf("Read new line")
	}

	if err := scanner.Err(); err != nil {
		log.Errorf("Error while reading file '%s': %s", fileName, err)
		return
	}
}

// WriteOut is the synchronized line to stdout
// it can also take a prepend filename, which is used to append the output to a given previous file, merging the column structure
func WriteOut(outlet <-chan *[]string, writerDone chan<- bool, prepend string) {
	log.Infof("Started writeOut Goroutine")
	// Needs to read line by line as to not introduce race conditions
	var transformation []int
	i := 0
	for outline := range outlet {
		log.Debugf("Got line %d: %s", i, outline)
		if transformation == nil {
			var err error
			// we can simply call prependPreviousOutputFile here without checking if a prepend file was given, as it
			// checks itself if a file is given and simply outputs the current header otherwise
			transformation, err = prependPreviousOutputFile(prepend, *outline)
			if err != nil {
				panic(err)
			}
			continue
		}
		outputLine(*outline, transformation)
		log.Debugf("Done with %d", i)
		i++
	}

	writerDone <- true
	close(writerDone)
}

// MergeFiles is a top-level functionality which takes a set of files and merges them by merging their headers and
// outputting them in order with appropriate transformations applied such that they result in one homogeneous format
func MergeFiles(fileNames []string) error {
	// load all files to read from
	scanners := make([]*bufio.Scanner, len(fileNames))
	for i, fileName := range fileNames {
		file, err := os.Open(fileName)
		if err != nil {
			return err
		}
		defer file.Close()

		scanners[i] = bufio.NewScanner(file)
	}

	// To do this merging we load all headers and subsequently calculate the transformations from "left-to-right", after
	// which we have the final header. Since the transformation preserves the previous header and only appends empty
	// fields to it we can simply pass the newly generated header along.
	// This can then be finished off by a second pass, which appends transformToEmpty to make all transformation the
	// length of the final header.
	//
	// 1 x n
	// |     load the first header as temporary header and assign an identity transformation to the first file
	//  \
	//   |   load the xth header and calculate transformations between temporary header and x, taking the resulting header as new temporary header
	//    \
	//     | load the nth header and calculate transformations between temporary header and n, taking the resulting header as final header
	// 1 x n
	// |     append transformToEmpty to fill to final length
	//  \
	//   |   append transformToEmpty to fill to final length
	//    \
	//     | no appending necessary, since this is already the final length

	// calculate all transformations
	transformations := make([][]int, len(fileNames))
	var header []string
	for i, scanner := range scanners {
		// read and parse header line
		if !scanner.Scan() {
			return fmt.Errorf("could not read header line from file %s", fileNames[i])
		}
		line, err := parseCSVLine(scanner.Text())
		if err != nil {
			return err
		}

		// skip transformations for first file and assign identity transformation
		if i == 0 {
			transformations[i] = make([]int, len(line))
			for j := range line {
				transformations[i][j] = j
			}
			header = line
			continue
		}

		// for all other files calculate the transformation to the current temporary header
		_, transformations[i], header = calculateTransformations(header, line)
	}

	// prepare a set of transformToEmpty which can be appended to all transformations that have too few fields
	headerLength := len(header)
	emptyTransformations := make([]int, headerLength-len(transformations[0]))
	for i := 0; i < len(emptyTransformations); i++ {
		emptyTransformations[i] = transformToEmpty
	}

	// append the emptyTransformations to all transformations that need more fields to get an evenly sized output
	for i := range transformations {
		appendLength := headerLength - len(transformations[i])
		transformations[i] = append(transformations[i], emptyTransformations[:appendLength]...)
	}

	// get an identity transformation for outputting the header and print it
	headerTrans := make([]int, len(header))
	for i := range headerTrans {
		headerTrans[i] = i
	}
	outputLine(header, headerTrans)

	// read all remaining file contents of all files and output them in order with the correct transformations applied
	for i, scanner := range scanners {
		for scanner.Scan() {
			line, err := parseCSVLine(scanner.Text())
			if err != nil {
				return err
			}

			if len(line) != 0 {
				outputLine(line, transformations[i])
			}
		}
	}

	return nil
}

// outputLine escapes and quotes a list of strings while transforming it according to the transformation list
// the result is printed to stdout
func outputLine(outline []string, transformation []int) {
	first := true
	for _, j := range transformation {
		if !first {
			fmt.Print(Separator)
		}
		first = false

		if j < 0 {
			continue // transformation index < 0 means that an empty field should be used
		}

		s := newlineEscape.Replace(outline[j])
		if strings.Contains(s, Separator) || strings.Contains(s, Quote) {
			s = Quote + strings.Replace(s, Quote, EscapedQuote, -1) + Quote
		}
		fmt.Print(s)
	}
	fmt.Println()
}

// prependPreviousOutputFile checks if a filename to be prepended was given, if not it returns the identity transformation
// if a filename was given the file is loaded and the header column is extracted to be merged with the current header column
// using calculateTransformations
// then the content of the prepend file is output with the given transformations applied
func prependPreviousOutputFile(fileName string, firstLine []string) ([]int, error) {
	// if no prepend file was given print the current header and return the identity transformation
	if fileName == "" {
		res := make([]int, len(firstLine))
		for i := 0; i < len(res); i++ {
			res[i] = i
		}
		outputLine(firstLine, res)
		return res, nil
	}

	// if a prepend file was given open it and create a line based scanner for it
	file, err := os.Open(fileName)
	if err != nil {
		log.Errorf("Could not open file '%s': %s", fileName, err)
		return nil, err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	// read and parse the first line
	if !scanner.Scan() {
		return nil, fmt.Errorf("prepend file %s is empty", fileName)
	}
	line, err := parseCSVLine(scanner.Text())
	if err != nil {
		return nil, fmt.Errorf("could not read header line from prepend file %s", fileName)
	}

	// calculate the transformations
	prependTrans, appendTrans, newHeader := calculateTransformations(line, firstLine)

	// create a null transformation for outputting the header
	headerTrans := make([]int, len(newHeader))
	for i := range headerTrans {
		headerTrans[i] = i
	}
	outputLine(newHeader, headerTrans)

	// output the rest of the prepend file using the prependTransformation
	for scanner.Scan() {
		line, err := parseCSVLine(scanner.Text())
		if err != nil {
			return nil, err
		}

		if len(line) != 0 {
			outputLine(line, prependTrans)
		}
	}

	return appendTrans, nil
}

// calculateTransformations takes a set of column definitions and calculates a combination as newHeader, whereby the columns
// from previous are in the beginning and all new columns of current are in the end
// it also calculates a transformation list for both the previous and current data to be used in outputLine
func calculateTransformations(previous, current []string) (prependTrans, appendTrans []int, newHeader []string) {
	// gather a set of available columns
	existing := make(map[string]struct{})
	for _, s := range previous {
		existing[s] = struct{}{}
	}
	for _, s := range current {
		existing[s] = struct{}{}
	}

	columnCount := len(existing)
	newHeader = make([]string, columnCount)
	prependTrans = make([]int, columnCount)
	appendTrans = make([]int, columnCount)

	// go through the previous header and give it an identity transformation for all existing fields as well as a
	// transformToEmpty for all fields that have to be newly appended, putting the column names directly in the new header
	for i := 0; i < columnCount; i++ {
		if i < len(previous) {
			prependTrans[i] = i
			newHeader[i] = previous[i]
		} else {
			prependTrans[i] = transformToEmpty
		}
		appendTrans[i] = transformToEmpty
	}

	// find all reordering of the current fields by checking for each header field where it is in the previous header
	// starting at the end of the previous header we find all fields in the current header that have not been present in
	// the previous header
	appendStart := len(previous)
	for i, c := range current {
		found := false
		for j, p := range previous {
			if c == p {
				appendTrans[j] = i
				found = true
			}
		}
		if !found {
			newHeader[appendStart] = c
			appendTrans[appendStart] = i
			appendStart++
		}
	}

	return prependTrans, appendTrans, newHeader
}

// parseCSVLine parses a single entry in a csv file to its primary components, with Separator as the column separator and Quote as quote
// while it supports escaped Quote characters inside a quoted field it does not support any other escape codes
func parseCSVLine(inline string) ([]string, error) {
	quoted := false
	escaped := false
	lastSeparator := 0

	lineLength := len(inline)
	start := 0
	result := make([]string, 0)
	for i, c := range inline {
		// allow skipping characters (used to skip separators after the end of a quoted field)
		if i < start {
			continue
		}

		// if we start a new field and the first character is a quote then enable quote mode and continue to next character
		if i == start && c == QuoteChar {
			quoted = true
			continue
		}

		if quoted {
			// if we are in quote mode we read until we hit another quote and properly unescape all contained quotes
			if c == EscapeChar {
				// as soon as we hit the escape character we enable escaped mode and continue to the next character
				escaped = true
				continue
			}

			if c == QuoteChar && !escaped {
				// if we hit an unescaped quote character we should have reached the end of the field
				if i+1 >= lineLength || int32(inline[i+1]) == SeparatorChar {
					// we check that by seeing if the next thing is either the end of the line or the proper separator character
					// if it is then append the result, skip the separator by jumping the start position and exit out of quote mode
					result = append(result, inline[start+1:i])
					start = i + 2
					lastSeparator = i + 1
					quoted = false
				} else {
					// if the field does not seem to end after the unescaped quote then throw an error
				   return nil, fmt.Errorf("could not parse CSV line at %d, invalid escape of quotes: %s", i, inline)
				}
			}

			// we can freely reset the escaped status when no quote character was hit, since we do not honor any escape codes
			escaped = false

		} else {
			// if we are unquoted we just look for the next separator
			if c == SeparatorChar {
				result = append(result, inline[start:i])
				start = i + 1
				lastSeparator = i
			}
		}
	}

	// only happens when the last field is unquoted, so we can take the entire rest of the line
	if start < lineLength {
		result = append(result, inline[start:])
	}

	// this happens when the last field is empty, as no value means that it is not handled in the loop and we need to add an empty field
	if lastSeparator == lineLength-1 {
		result = append(result, "")
	}

	return result, nil
}
