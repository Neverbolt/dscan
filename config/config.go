package config

var (
	// Separator is the separator for the csv file
	Separator = ";"
	// SeparatorChar is the separator character for the csv file
	SeparatorChar = ';'
	// InnerSeparator is the separator for lists in fields
	InnerSeparator = ","
	// Quote is the character that is used for quoting fields in CSV as string
	Quote = "\""
	// QuoteChar is the character that is used for quoting fields in CSV
	QuoteChar = '"'
	// EscapeChar is the character that is used to escape elements in CSV
	EscapeChar = '\\'
	// EscapedQuote is the replacement for Quote inside a quoted field
	EscapedQuote = "\\\""
)

// Config is the culmination of all configurations for the current scan
type Config interface {
	Int(string) int
	Bool(string) bool
	String(string) string
	StringSlice(string) []string
}
