package config

import (
	"strings"

	log "github.com/Sirupsen/logrus"
)

// Host is the struct that contains all information about a target
type Host struct {
	// The original domain
	Testing string

	// the result after following all redirects
	// if online is false then redirection following did not work/the host is offline
	Online  bool
	Domain  string
	URL     string

	// other values given in the domain file
	Cookies map[string]string
	Headers map[string]string
	Vars    map[string]string
}

// ParseHost parses a single line of a host input file
func ParseHost(line string) Host {
	parts := strings.Split(line, Separator)
	domain := parts[0]

	host := Host{Testing: domain}

	if len(parts) > 1 {
		for _, part := range parts {
			atoms := strings.SplitN(part, ":", 2)

			if len(atoms) != 2 {
				log.Errorf("Invalid input line part for domain %s: '%s'\n", domain, part)
			}

			if atoms[0] == "cookie" {
				host.Cookies = ParseParams(atoms[1], domain)
			} else if atoms[0] == "header" {
				host.Headers = ParseParams(atoms[1], domain)
			} else if atoms[0] == "var" {
				host.Vars = ParseParams(atoms[1], domain)
			} else {
				log.Errorf("Invalid domain param type for domain %s: '%s'", domain, atoms[0])
			}
		}
	}

	return host
}

// ParseParams parses the parameters that can be supplied per domain
func ParseParams(param, domain string) map[string]string {
	if param[0] == '"' && param[len(param)-1] == '"' {
		param = param[1 : len(param)-2]
	}

	paramStrings := strings.Split(param, InnerSeparator)
	params := make(map[string]string, len(paramStrings))

	for _, param := range paramStrings {
		parts := strings.SplitN(param, ":", 2)

		if len(parts) != 2 {
			log.Errorf("Invalid param for domain %s: '%s'", domain, param)
			continue
		}

		params[parts[0]] = parts[1]
	}

	return params
}
